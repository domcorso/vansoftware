<div class="panel" data-panel-name="trips.list">
    <div class="panel__header">
        <div class="panel__title">All Trips</div>
        <div class="panel__buttons">
            <button class="panel__button trips-btn-create" type="button">
                <i class="icon ion-ios-add"></i>
                Create
            </button>
        </div>
    </div>
    <div class="panel__content-wrapper">
        <table class="datatable" id="trips.datatable.list">
            <thead>
                <tr></tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>

<div class="panel" data-panel-name="trips.view">
    <div class="panel__header">
        <div class="panel__title"></div>
        <div class="panel__buttons">
            <button class="panel__button trips-btn-list" type="button">
                <i class="icon ion-ios-list"></i>
                All Trips
            </button>
            <button class="panel__button panel__button--warning trips-btn-edit" type="button">
                <i class="icon ion-ios-create"></i>
                Edit
            </button>
        </div>
    </div>
    <div class="panel__content-wrapper">
        <div class="column-layout column-layout--padding">
            <div class="column-layout__col column-layout__col--6">
                <h1 class="panel__heading no-margin-top" id="trips.view.name"></h1>
                <h3 class="panel__heading" id="trips.view.tripdates"></h3>
                <p class="panel__text" id="trips.view.description"></p>
            </div>
            <div class="column-layout__col column-layout__col--6">
                <div class="summary-widget" id="trips.view.usersummary">
                    <div class="summary-widget__title">
                        <i class="icon ion-md-contacts"></i>
                        Users
                    </div>
                    <div class="summary-widget__text"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel" data-panel-name="trips.create-edit">
    <div class="panel__header">
        <div class="panel__title"></div>
        <div class="panel__buttons">
            <button class="panel__button trips-btn-list" type="button">
                <i class="icon ion-ios-list"></i>
                All Trips
            </button>
            <button class="panel__button panel__button--success trips-btn-save" type="button">
                <i class="icon ion-ios-checkmark-circle"></i>
                Save
            </button>
        </div>
    </div>
    <div class="panel__content-wrapper">
        <div class="standard-form">
            <div class="column-layout column-layout--padding">
                <div class="column-layout__col column-layout__col--6">
                    <label>Name *</label>
                    <input type="text" id="trips.input.name">

                    <label>Start Date *</label>
                    <input type="text" id="trips.input.startdate">

                    <label>End Date</label>
                    <input type="text" id="trips.input.enddate">
                </div>
                <div class="column-layout__col column-layout__col--6">
                    <label>Description</label>
                    <textarea id="trips.input.description"></textarea>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel" data-panel-name="trips.edit-users">
    <div class="panel__header">
        <div class="panel__title">Edit Users</div>
        <div class="panel__buttons">
            <button class="panel__button trips-btn-back-to-trip" type="button">
                <i class="icon ion-md-arrow-back"></i>
                Back
            </button>
            <button class="panel__button panel__button--success trips-btn-save-users" type="button">
                <i class="icon ion-ios-checkmark-circle"></i>
                Save
            </button>
        </div>
    </div>
    <div class="panel__content-wrapper">
        <table class="datatable" id="trips.datatable.editusers">
            <thead>
                <tr></tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>
