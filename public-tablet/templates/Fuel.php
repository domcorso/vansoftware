<style>
    #fuel\.view\.map {
        width: 100%;
        height: 200px; /* Really need to refactor the panels :/ */
        margin-top: 40px;
    }
</style>

<div class="panel" data-panel-name="fuel.list">
    <div class="panel__header">
        <div class="panel__title">Fuel History</div>
    </div>
    <div class="panel__content-wrapper">
        <table class="datatable" id="fuel.datatable.list">
            <thead>
                <tr></tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>

<div class="panel" data-panel-name="fuel.view">
    <div class="panel__header">
        <div class="panel__title">View Fuel Entry</div>
        <div class="panel__buttons">
            <button class="panel__button fuel-btn-list" type="button">
                <i class="icon ion-md-arrow-back"></i>
               Back
            </button>
        </div>
    </div>
    <div class="panel__content-wrapper">
        <div class="column-layout column-layout--padding">
            <div class="column-layout__col column-layout__col--6">
                <h1 class="panel__heading no-margin-top">
                    Litres<span id="fuel.view.litres" class="no-bold float-right">00.00</span><br>
                    Per Litre<span id="fuel.view.costperlitre" class="no-bold float-right">$0.000/L</span><br>
                    Total Cost<span id="fuel.view.totalcost" class="no-bold float-right">$0.00</span>
                </h1>
                <h2 class="panel__heading no-bold">
                    <span id="fuel.view.createdat"></span><br>
                    by <span id="fuel.view.enteredby"></span>
                </h2>
                <div class="summary-widget" id="fuel.view.viewmap">
                    <div class="summary-widget__title">
                        <i class="icon ion-md-map"></i>
                        View Map
                    </div>
                </div>
            </div>
            <div class="column-layout__col column-layout__col--6">
                <img class="panel__image panel__image--right panel__image--border" id="fuel.view.receiptphoto" src="" alt="">
            </div>
        </div>
    </div>
</div>

<div class="panel panel--no-scroll" data-panel-name="fuel.viewmap">
    <div class="panel__header">
        <div class="panel__title">MAP VIEW</div>
        <div class="panel__buttons">
            <button class="panel__button fuel-btn-map-back" type="button">
                <i class="icon ion-md-arrow-back"></i>
                Back
            </button>
        </div>
    </div>
    <div class="panel__content-wrapper">
        <div class="column-layout column_layout--100-height">
            <div class="column-layout__col">
                <div id="fuel.viewmap.map" style="width: 100%; height: 100%"></div>
            </div>
        </div>
    </div>
</div>
