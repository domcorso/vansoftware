<?php

require_once realpath(__DIR__ . "/../../app/Bootstrap.php");

$appListFileContents = file_get_contents(RESOURCES . "/apps/app-list.json");
$appList = json_decode($appListFileContents);

?>

<div class="swiper-container">
    <div class="swiper-wrapper">
        <div class="swiper-slide">
            <div class="apps-container">
                <?php foreach ($appList as $app) { ?>
                    <?php
                        if ($app->id === "AppDrawer") {
                            continue;
                        }
                    ?>

                    <div class="app-icon">
                        <img data-app-id="<?= $app->id ?>" src="/images/app-icons/<?= $app->id ?>.png" alt="App Name" class="app-icon__image rounded-button">
                    <div class="app-icon__name"><?= $app->title ?></div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
