<?php

require_once realpath(__DIR__ . "/../../app/Bootstrap.php");

header("Content-type: text/plain");

$result = $app->logout();

echo $result === true ? "1" : "0";
