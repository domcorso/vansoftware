<?php

require_once realpath(__DIR__ . "/../../app/Bootstrap.php");

header("Content-type: text/plain");

if (!isset($_POST["passcode"])) {
    http_response_code(400);
    die("No passcode was given.");
}

$givenPasscode = $_POST["passcode"];
$result = $app->loginAsTablet($givenPasscode);

echo $result === true ? "1" : "0";
