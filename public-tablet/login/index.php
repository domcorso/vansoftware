<?php

require_once realpath(__DIR__ . "/../../app/Bootstrap.php");

# Redirect to tablet page if already logged in as tablet
if ($app->loggedInAsTablet()) {
    header('Location: /');
    die('You are already logged in as the tablet.');
}

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Login - Van Software - Unlucky Van</title>
        <link rel="stylesheet" href="/css/fonts.css">
        <link rel="stylesheet" href="/css/colors.css">
        <link rel="stylesheet" href="/css/login/login.css">
    </head>
    <body>
        <div class="login-container">
            <div class="login-container__passcode"></div>
            <div class="login-container__keypad">
                <button class="login-container__button">1</button>
                <button class="login-container__button">2</button>
                <button class="login-container__button">3</button><br>
                <button class="login-container__button">4</button>
                <button class="login-container__button">5</button>
                <button class="login-container__button">6</button><br>
                <button class="login-container__button">7</button>
                <button class="login-container__button">8</button>
                <button class="login-container__button">9</button>
            </div>
        </div>
        <script>
            const Login = {
                e: {
                    passCode: null,
                    buttons: []
                },

                passCode: '',

                init() {
                    this.e.passCode = document.querySelector('.login-container__passcode')
                    this.e.buttons = Array.from(document.querySelectorAll('.login-container__button'))

                    for (const button of this.e.buttons) {
                        button.addEventListener('click', e => {
                            this.passCodeAppend(button.textContent)
                        })
                    }

                    this.e.passCode.textContent = ''
                },

                passCodeAppend(char) {
                    if (char.length === 1 && this.passCode.length < 4) {
                        this.passCode += char
                        this.e.passCode.textContent = ''

                        for (const i of this.passCode) {
                            this.e.passCode.textContent += '*'
                        }

                        if (this.passCode.length === 4) {
                            setTimeout(() => this.attemptLogin(), 250)
                        }
                    }
                },

                clearPasscode() {
                    this.passCode = ''
                    this.e.passCode.textContent = ''
                },

                attemptLogin() {
                    fetch('/login/authenticate.php', {
                        method: 'post',
                        credentials: 'same-origin',
                        body: `passcode=${this.passCode}`,
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        }
                    }).then(response => {
                        return response.text()
                    }).then(text => {
                        if (text == '1') {
                            window.location.href = '/'
                        } else {
                            this.clearPasscode()
                        }
                    })
                }
            }

            document.addEventListener('DOMContentLoaded', function() {
                Login.init()
            })
        </script>
    </body>
</html>
