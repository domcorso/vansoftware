<?php

require_once "../app/Bootstrap.php";

# Redirect to login page if not logged in
if (!$app->loggedInAsTablet()) {
    header("Location: /login");
    die("You need to be logged in as tablet for this.");
}

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Van Software - Unlucky Van</title>
        <link rel="stylesheet" href="/lib/swiper/swiper.min.css">
        <link rel="stylesheet" href="/lib/ionicons/ionicons.min.css">
        <link rel="stylesheet" href="/lib/material-datepicker/material-datetime-picker.css">
        <link rel="stylesheet" href="/lib/material-icons/material-icons.css">
        <link rel="stylesheet" href="/css/main.css">
    </head>
    <body>
        <div id="main">
            <div id="app-container"></div>
            <div id="sidebar">
                <div id="sidebar__top">
                    <div id="clock">
                        <div id="clock-time">00:00 pp</div>
                        <div id="clock-ampm"></div>
                    </div>
                </div>
                <div id="sidebar__bottom">
                    <button id="btn-app-drawer" class="rounded-button">
                        <i class="icon ion-ios-keypad"></i>
                    </button>
                </div>
            </div>
        </div>
        <script src="/lib/swiper/swiper.min.js"></script>
        <script src="/lib/momentjs/moment.min.js"></script>
        <script src="/lib/rome/rome.standalone.min.js"></script>
        <script src="/lib/material-datepicker/material-datetime-picker.js"></script>
        <script src="/js/main.bundle.js"></script>
    </body>
</html>
