# Build Notes!

### Installing Tesseract
* Tesseract OCR needs to be installed: https://github.com/tesseract-ocr/tesseract
* The command "tesseract" needs to be in the PATH
* Add TESSDATA_PREFIX to environment variable (it's the ./tessdata directory in Tesseract folder)
