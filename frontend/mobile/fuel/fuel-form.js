import _$ from 'lib/globalhelper'

const FuelForm = {
    e: {
        inpLitres: null,
        inpCostPerLitre: null,
        inpTotalCost: null,
        inpReceiptPhoto: null,
        inpReceiptPhotoFilename: null,
        btnCaptureReceipt: null,
        imgReceiptPhoto: null
    },

    UPLOAD_RECEIPT_URL: '/api/fuel-entries/upload__receipt-photo.php',

    init() {
        this.getElements()

        for (const inp of [this.e.inpLitres, this.e.inpCostPerLitre, this.e.inpTotalCost]) {
            inp.addEventListener('change', e => this.eagerCalculateFields())
        }

        this.e.btnCaptureReceipt.addEventListener('click', e => this.e.inpReceiptPhoto.click())
        this.e.inpReceiptPhoto.addEventListener('change', e => {
            if (this.e.inpReceiptPhoto.files.length > 0) {
                this.uploadReceiptPhoto()
            }
        })
    },

    getElements() {
        this.e.inpLitres = _$.byId('inpLitres')
        this.e.inpCostPerLitre = _$.byId('inpCostPerLitre')
        this.e.inpTotalCost = _$.byId('inpTotalCost')
        this.e.btnCaptureReceipt = _$.byId('btnCaptureReceipt')

        // Create input for uploading receipt photo
        this.e.inpReceiptPhoto = _$.elm('input')
        this.e.inpReceiptPhoto.setAttribute('type', 'file')

        this.e.inpReceiptPhotoFilename = _$.byId('inpReceiptPhotoFilename')
        this.e.imgReceiptPhoto = _$.byId('imgReceiptPhoto')
    },

    eagerCalculateFields() {
        const litres = parseFloat(this.e.inpLitres.value)
        const costPerLitre = parseFloat(this.e.inpCostPerLitre.value)
        const totalCost = parseFloat(this.e.inpTotalCost.value)

        if (isNaN(litres) && !isNaN(costPerLitre) && !isNaN(totalCost)) {
            this.e.inpLitres.value = (totalCost / costPerLitre).toFixed(2)
        }

        if (isNaN(costPerLitre) && !isNaN(litres) && !isNaN(totalCost)) {
            this.e.inpCostPerLitre.value = (totalCost / litres).toFixed(3)
        }

        if (isNaN(totalCost) && !isNaN(litres) && !isNaN(costPerLitre)) {
            this.e.inpTotalCost.value = (litres * costPerLitre).toFixed(2)
        }
    },

    uploadReceiptPhoto() {
        const fileInput = this.e.inpReceiptPhoto
        const formData = new FormData()

        formData.append('receipt_photo', fileInput.files[0])

        _$.cfetch(this.UPLOAD_RECEIPT_URL, {
            method: 'post',
            body: formData
        }).then(response => {
            return response.json()
        }).then(data => {
            this.handleReceiptPhotoResponse(data)
        }).catch(error => {
            console.error(error)
        })
    },

    handleReceiptPhotoResponse(data) {
        this.e.inpLitres.value = data.scanned_values.litres > 0 ? data.scanned_values.litres.toFixed(2) : ''
        this.e.inpCostPerLitre.value = data.scanned_values.cost_per_litre > 0 ? data.scanned_values.cost_per_litre.toFixed(3) : ''
        this.e.inpTotalCost.value = data.scanned_values.total_cost ? data.scanned_values.total_cost.toFixed(2) : ''

        this.e.inpReceiptPhotoFilename.value = data.filename

        this.e.imgReceiptPhoto.src = data.uploaded_url
        this.e.imgReceiptPhoto.classList.remove('hidden')
    }
}

document.addEventListener('DOMContentLoaded', () => FuelForm.init())
