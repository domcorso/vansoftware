import _$ from 'lib/globalhelper'

const MobileMain = {
    e: {
        btnNavigation: null,
        btnUserSettings: null,
        navigationMenu: null,
        header: null,
        navigationOverlay: null
    },

    init() {
        this.getElements()
        this.initNavigation()
        this.activateHrefs()
    },

    getElements() {
        this.e.btnNavigation = _$.byId('btn-open-nav')
        this.e.btnUserSettings = _$.byId('btn-user-settings')
        this.e.navigationMenu = _$.byId('navigation')
        this.e.header = _$.sel('.header')
        this.e.navigationOverlay = _$.byId('navigation-overlay')
    },

    initNavigation() {
        const toggle = () => this.e.navigationMenu.classList.toggle('navigation--open')

        this.e.btnNavigation.addEventListener('click', e => toggle())
        this.e.navigationOverlay.addEventListener('click', e => toggle())
    },

    activateHrefs() {
        _$.click('[data-href]', function() {
            window.location.href = this.dataset.href
        })
    }
}

document.addEventListener('DOMContentLoaded', () => MobileMain.init())
