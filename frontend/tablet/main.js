import _$ from 'lib/globalhelper'
import moment from 'moment'

// Modules
import AppSwitcher from './modules/AppSwitcher'
import PanelSwitcher from './modules/PanelSwitcher'
import Alerts from './modules/Alerts'

// All apps
import AppDrawer from './apps/AppDrawer'
import Trips from './apps/Trips'
import Fuel from './apps/Fuel'

const App = {
    e: {
        clock: {
            time: null,
            ampm: null
        },
        btnAppDrawer: null
    },

    appSwitcher: null,
    panelSwitcher: PanelSwitcher,

    async init() {
        window.app = this

        this.getElements()
        this.startClock()
        this.panelSwitcher.init()

        this.appSwitcher = await AppSwitcher.init({
            AppDrawer,
            Trips,
            Fuel
        })

        this.alerts = Alerts.init()
        
        this.e.btnAppDrawer.addEventListener('click', e => this.appSwitcher.open('AppDrawer'))
        this.e.btnAppDrawer.addEventListener('touchstart', e => {
            const startTime = Date.now()

            // If held for longer than half a second, force reload of current app
            e.target.addEventListener('touchend', e => {
                if (Date.now() - startTime > 500) {
                    e.preventDefault()
                    
                    if (this.appSwitcher.activeApp) {
                        this.appSwitcher.open(this.appSwitcher.activeApp.id, true)
                    }
                }
            }, { once: true })
        })
    },

    getElements() {
        this.e.clock.time = _$.byId('clock-time')
        this.e.clock.ampm = _$.byId('clock-ampm')
        this.e.btnAppDrawer = _$.byId('btn-app-drawer')
    },

    startClock() {
        const showTime = () => {
            this.e.clock.time.textContent = moment().format('h:mm')
            this.e.clock.ampm.textContent = moment().format('A')
        }

        showTime()
        setInterval(() => showTime(), 1000)
    },

    fatalError(message, e) {
        console.warn(`Error: ${message}`)
        console.error(e)
    }
}

document.addEventListener('DOMContentLoaded', () => App.init())
