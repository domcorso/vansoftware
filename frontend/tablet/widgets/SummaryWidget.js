import _$ from 'lib/globalhelper'

class SummaryWidget {
    constructor (elementOrId) {
        if (typeof elementOrId === "string") {
            this.elmRoot = _$.byId(elementOrId)
        } else {
            this.elmRoot = element
        }

        this.elmTitle = this.elmRoot.querySelector('.summary-widget__title')
        this.elmText = this.elmRoot.querySelector('.summary-widget__text')
    }

    onClick(listener) {
        this.elmRoot.addEventListener('click', listener)
    }

    reset() {
        this.elmText.classList.remove('summary-widget__text--image-list')
        _$.emptyElem(this.elmText)
    }

    imageList(images = [], messageIfEmpty = 'NONE') {
        this.reset()

        if (images.length === 0) {
            this.elmText.textContent = messageIfEmpty
            return
        }

        this.elmText.classList.add('summary-widget__text--image-list')
        for (const imageData of images) {
            const img = _$.elm('img')
            const src = imageData.src
            const borderColor = imageData.borderColor

            img.classList.add('summary-widget__image')
            img.src = src
            img.style.borderColor = borderColor

            this.elmText.appendChild(img)
        }
    }
}

export default SummaryWidget
