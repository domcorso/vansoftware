// Global: Swiper

import _$ from 'lib/globalhelper'

const AppDrawer = {
    appBtns: [],
    swiper: null
}

AppDrawer.init = function() {
    this.swiper = new Swiper('[data-app="AppDrawer"] .swiper-container')

    this.appBtns = document.querySelectorAll('[data-app-id]')
    this.appBtns.forEach(appBtn => {
        appBtn.addEventListener('click', e => this.appBtnClicked(appBtn))
    })
}

AppDrawer.appBtnClicked = function (appBtn) {
    window.app.appSwitcher.open(appBtn.dataset.appId)
}

AppDrawer.terminate = function() {
    
}

export default AppDrawer
