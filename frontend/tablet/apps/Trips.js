import _$ from 'lib/globalhelper'
import moment from 'moment'
import DataTable from '../modules/DataTable'
import SummaryWidget from '../widgets/SummaryWidget'

const Trips = {
    e: {
        inputFields: {
            tripName: null,
            tripDescription: null,
            startDate: null,
            endDate: null,
            archived: null
        },
        view: {
            tripName: null,
            tripDescription: null,
            tripDates: null
        },
        datatables: {
            list: null
        }
    },

    widgets: {
        view: {
            userSummary: null
        }
    },

    currentAction: null,
    tripInView: null,

    endPoints: new Map([
        ['list', '/api/trips/retrieve__trips.php'],
        ['create', '/api/trips/create__trip.php'],
        ['update', '/api/trips/update__trip.php'],
        ['all-users', '/api/users/retrieve__users.php'],
        ['trip-user-link', '/api/trips/linkusers__trip.php']
    ])
}

Trips.init = function() {
    this.getElements()
    this.initializeWidgets()

    // Activate all buttons
    _$.click('.trips-btn-create', e => this.create())
    _$.click('.trips-btn-list', e => this.list())
    _$.click('.trips-btn-save', e => this.save())
    _$.click('.trips-btn-edit', e => this.edit(this.tripInView.id))
    _$.click('.trips-btn-back-to-trip', e => this.view(this.tripInView.id))
    _$.click('.trips-btn-save-users', e => this.saveUsers())

    // Widgets
    this.widgets.view.userSummary.onClick(e => this.editUsers(this.tripInView.id))

    // Activates a MaterialDatetimePicker for these input fields
    for (const inputField of [this.e.inputFields.startDate, this.e.inputFields.endDate]) {
        const picker = new MaterialDatetimePicker()

        picker.on('submit', value => {
            const momentDate = moment(value)
            const formattedDate = momentDate.format('DD/MM/YYYY')

            inputField.value = formattedDate

            // Set the 'End Date' to 1 day more than the 'Start Date' if that is given
            if (inputField === this.e.inputFields.startDate && this.e.inputFields.endDate.value === '') {
                this.e.inputFields.endDate.value = momentDate.add(1, 'days').format('DD/MM/YYYY')
            }
        })

        inputField.addEventListener('click', e => {
            inputField.blur()
            picker.open()
        })
    }

    this.list()
}

Trips.getElements = function() {
    this.e.datatables.list = _$.byId('trips.datatable.list')
    this.e.datatables.editUsers = _$.byId('trips.datatable.editusers')

    this.e.inputFields.tripId = _$.byId('trips.input.id')
    this.e.inputFields.tripName = _$.byId('trips.input.name')
    this.e.inputFields.tripDescription = _$.byId('trips.input.description')
    this.e.inputFields.startDate = _$.byId('trips.input.startdate')
    this.e.inputFields.endDate = _$.byId('trips.input.enddate')
    this.e.inputFields.archived = _$.byId('trips.input.archived')

    this.e.view.tripName = _$.byId('trips.view.name')
    this.e.view.tripDescription = _$.byId('trips.view.description')
    this.e.view.tripDates = _$.byId('trips.view.tripdates')
}

Trips.initializeWidgets = function() {
    this.widgets.view.userSummary = new SummaryWidget('trips.view.usersummary')
}

Trips.list = async function() {
    let trips = []

    try {
        trips = await ( await _$.cfetch(this.endPoints.get('list')) ).json()
    } catch (e) {
        window.app.fatalError('Could not load trips!', e)
    }

    trips = trips.map(trip => {
        return {
            allData: trip,
            displayCells: [
                trip.name,
                moment(trip.startDate.date).format("MMM D, YYYY")
            ]   
        }
    })

    // Populate data table - on row click, run 'view' method, passing
    // in the selected trip ID
    DataTable.setHeaders(this.e.datatables.list, ['Name', 'Date'])
    DataTable.populate(this.e.datatables.list, trips, (e, allData) => this.view(allData.id))
    
    window.app.panelSwitcher.showPanel('trips.list')
    this.currentAction = 'list'
}

Trips.view = async function(tripId) {
    let trip = null
    try {
        trip = await this.getTripById(tripId, true)
    } catch (e) {
        window.app.alerts.push({
            type: 'warning',
            title: 'Could not find trip!',
            buttons: [{ type: 'ok' }]
        })

        return
    }

    // Construct trip dates text (i.e. Jan 1 1900 - Jan 19 1900)
    let tripDatesText = ''
    tripDatesText += moment(trip.startDate.date).format("MMM D, YYYY")
    tripDatesText += trip.endDate ? ' - ' + moment(trip.endDate.date).format("MMM D, YYYY") : ''

    this.e.view.tripName.textContent = trip.name
    this.e.view.tripDescription.textContent = trip.description
    this.e.view.tripDates.textContent = tripDatesText

    this.widgets.view.userSummary.imageList(trip.users.map(user => {
        return {
            borderColor: user.color,
            src: user.profilePhoto
        }
    }), 'TAP TO ADD')

    this.tripInView = trip

    window.app.panelSwitcher.showPanel('trips.view')
    window.app.panelSwitcher.setTitle('trips.view', 'Trip Details')

    this.currentAction = 'view'
}

Trips.create = function() {
    this.resetInput()

    this.e.inputFields.startDate.value = moment().format("DD/MM/YYYY")

    window.app.panelSwitcher.showPanel('trips.create-edit')
    window.app.panelSwitcher.setTitle('trips.create-edit', 'Create Trip')

    this.currentAction = 'create'
}

Trips.edit = async function(tripId) {
    this.resetInput()

    let trip = null
    try {
        trip = await this.getTripById(tripId)
    } catch (e) {
        window.app.alerts.push({
            type: 'warning',
            title: 'Could not find trip!',
            buttons: [{ type: 'ok' }]
        })

        return
    }

    this.tripInView = trip

    this.e.inputFields.tripName.value = trip.name
    this.e.inputFields.tripDescription.value = trip.description
    this.e.inputFields.startDate.value = moment(trip.startDate.date).format("DD/MM/YYYY")
    this.e.inputFields.endDate.value = trip.endDate ? moment(trip.endDate.date).format("DD/MM/YYYY") : ''

    window.app.panelSwitcher.showPanel('trips.create-edit')
    window.app.panelSwitcher.setTitle('trips.create-edit', 'Edit Trip')

    this.currentAction = 'update'
}

Trips.save = function() {
    if (this.currentAction === 'create' || this.currentAction === 'update') {
        const endpoint = this.endPoints.get(this.currentAction)
        const payload = this.constructPayload(this.currentAction === 'update')

        const init = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(payload)
        }

        _$.cfetch(endpoint, init).then(response => {
            return response.json()
        }).then(data => {
            this.handleSaveResponse(data)
        }).catch(error => {
            window.app.fatalError(error)
        })
    } else {
        console.warn('Attempt was made to save a Trip but not on create or update view!')
    }
}

Trips.editUsers = async function(tripId) {
    let trip = null
    let allUsers = null

    // Get data for the current Trip
    try {
        trip = await this.getTripById(tripId, true)
    } catch (e) {
        window.app.alerts.push({
            type: 'warning',
            title: 'Could not find trip!',
            buttons: [{ type: 'ok' }]
        })

        return
    }

    // Get all Users in the system
    try {
        allUsers = await ( await _$.cfetch(this.endPoints.get('all-users')) ).json()
    } catch (e) {
        window.app.alerts.push({
            type: 'warning',
            title: 'Could not get list of Users!',
            buttons: [{ type: 'ok' }]
        })

        return
    }

    this.tripInView = trip

    const usersDt = allUsers.map(user => {
        return {
            allData: user,
            displayCells: [
                user.displayName
            ],
            initSelected: trip.users.some(tripUser => tripUser.id === user.id)
        }
    })

    const tableHeader = `Who's coming to ${trip.name}?`

    DataTable.setHeaders(this.e.datatables.editUsers, [tableHeader])
    DataTable.populate(this.e.datatables.editUsers, usersDt, e => {}, true)

    window.app.panelSwitcher.showPanel('trips.edit-users')
    this.currentAction = 'edit-users'
}

Trips.saveUsers = function() {
    if (this.currentAction === 'edit-users') {
        const endpoint = this.endPoints.get('trip-user-link')
        const payload = {
            'tripId': this.tripInView.id,
            'users': DataTable.getRows(this.e.datatables.editUsers).map(tr => {
                return {
                    'userId': JSON.parse(tr.dataset.allData).id,
                    'action': DataTable.isRowSelected(tr) ? 'add' : 'remove'
                }
            })
        }

        const init = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(payload)
        }

        _$.cfetch(endpoint, init).then(response => {
            return response.json()
        }).then(data => {
            this.handleSaveResponse(data)
        }).catch(error => {
            window.app.fatalError(error)
        })
    } else {
        console.warn('Attempt was made to edit users for a Trip but not on edit-users view!')
    }
}

Trips.constructPayload = function(update = false) {
    const payload = {
        'tripName': this.e.inputFields.tripName.value,
        'tripDesc': this.e.inputFields.tripDescription.value,
        'startDate': this.e.inputFields.startDate.value,
        'endDate': this.e.inputFields.endDate.value
    }

    if (update) {
        payload['tripId'] = this.tripInView.id
    }

    return payload
}

Trips.handleSaveResponse = function(data) {
    const successful = data.errors.length === 0
    const alertBox = {}

    if (successful) {
        alertBox.type = 'success'
        alertBox.title = 'Trip Saved!'
        alertBox.buttons = [
            { type: 'ok' }
        ]

        if (this.currentAction === 'create') {
            this.create()
            alertBox.buttons.push({
                type: 'external',
                callback: () => {
                    this.edit(data.tripId)
                }
            })
        }
    } else {
        alertBox.type = 'error'
        alertBox.title = 'Can\'t save Trip!'
        alertBox.message = _$.arrayToHtmlList(data.errors)
        alertBox.buttons = [
            { type: 'ok' }
        ]
    }

    window.app.alerts.push(alertBox)
}

Trips.getTripById = async function(tripId, includeUsers = false) {
    const queryString = new URLSearchParams({
        trip_id: tripId || 0,
        link: includeUsers ? 'user' : ''
    }).toString()
    
    const url = `${this.endPoints.get('list')}?${queryString}`
    const serverResponse = await ( await _$.cfetch(url) ).json()

    if (serverResponse.length === 1) {
        return serverResponse[0]
    } throw `Trip not found with an ID of: ${tripId}`
}

Trips.resetInput = function() {
    _$.clearInputFields(Object.values(this.e.inputFields))
}

Trips.terminate = function() {
    this.resetInput()
}

export default Trips
