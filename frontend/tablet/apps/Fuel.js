import _$ from 'lib/globalhelper'
import moment from 'moment'
import DataTable from '../modules/DataTable'
import SummaryWidget from '../widgets/SummaryWidget'

const Fuel = {
    e: {
        view: {
            litres: null,
            costPerLitre: null,
            totalCost: null,
            createdAt: null,
            enteredBy: null,
            imgReceiptPhoto: null
        },

        viewmap: {
            map: null
        },

        datatables: {
            list: null
        }
    },

    widgets: {
        view: {
            viewMap: null
        }
    },

    currentAction: null,
    fuelEntryInView: null,

    endPoints: new Map([
        ['list', '/api/fuel-entries/retrieve__fuel-entries.php']
    ])
}

Fuel.init = function() {
    this.getElements()

    // Activate all buttons
    _$.click('.fuel-btn-list', e => this.list())
    _$.click('.fuel-btn-map-back', e => this.view(this.fuelEntryInView.id))

    // Widgets
    this.widgets.view.viewMap.onClick(e => this.viewMap())

    this.list()
}

Fuel.getElements = function() {
    this.e.datatables.list = _$.byId('fuel.datatable.list')
    this.e.view.litres = _$.byId('fuel.view.litres')
    this.e.view.costPerLitre = _$.byId('fuel.view.costperlitre')
    this.e.view.totalCost = _$.byId('fuel.view.totalcost')
    this.e.view.createdAt = _$.byId('fuel.view.createdat')
    this.e.view.enteredBy = _$.byId('fuel.view.enteredby')
    this.e.view.imgReceiptPhoto = _$.byId('fuel.view.receiptphoto')

    this.e.viewmap.map = _$.byId('fuel.viewmap.map')

    this.widgets.view.viewMap = new SummaryWidget('fuel.view.viewmap')
}

Fuel.list = async function() {
    let fuelEntries = []

    try {
        fuelEntries = await ( await _$.cfetch(this.endPoints.get('list')) ).json()
    } catch (e) {
        window.app.fatalError('Could not load fuel entries!', e)
    }

    fuelEntries = fuelEntries.map(fuelEntry => {
        return {
            allData: fuelEntry,
            displayCells: [
                moment(fuelEntry.createdAt.date).format('ddd Do MMM YYYY @ h:mma'),
                '$' + fuelEntry.totalCost.toFixed(2)
            ]
        }
    })

    DataTable.setHeaders(this.e.datatables.list, ['Date & Time', 'Total'])
    DataTable.populate(this.e.datatables.list, fuelEntries, (e, allData) => this.view(allData.id))

    window.app.panelSwitcher.showPanel('fuel.list')
    this.currentAction = 'list'
}

Fuel.view = async function(fuelEntryId) {
    let fuelEntry = null;
    try {
        fuelEntry = await this.getFuelEntryById(fuelEntryId)
    } catch (e) {
        window.app.alerts.push({
            type: 'warning',
            title: 'Could not find fuel entry!',
            buttons: [{ type: 'ok' }]
        })

        return
    }

    this.e.view.litres.textContent = fuelEntry.litres.toFixed(2) + 'L'
    this.e.view.costPerLitre.textContent = '$' + fuelEntry.costPerLitre.toFixed(3) + '/L'
    this.e.view.totalCost.textContent = '$' + fuelEntry.totalCost.toFixed(2)
    this.e.view.createdAt.textContent = moment(fuelEntry.createdAt.date).format('ddd Do MMM YYYY @ h:mma')
    this.e.view.enteredBy.textContent = fuelEntry.enteredBy.displayName

    if (fuelEntry.receiptPhotoUrl.length > 0) {
        this.e.view.imgReceiptPhoto.classList.remove('hidden')
        this.e.view.imgReceiptPhoto.src = fuelEntry.receiptPhotoUrl
    } else {
        this.e.view.imgReceiptPhoto.classList.add('hidden')
    }

    this.fuelEntryInView = fuelEntry

    window.app.panelSwitcher.showPanel('fuel.view')

    this.currentAction = 'view'
}

Fuel.viewMap = function() {
    alert("Not Implemented");
    return;
    
    // Don't do anything if there is currently no fuel entry in view
    if (!this.fuelEntryInView || !this.fuelEntryInView.latitude) {
        return
    }

    const location = {
        lat: this.fuelEntryInView.latitude,
        lng: this.fuelEntryInView.longitude
    }

    window.app.panelSwitcher.showPanel('fuel.viewmap')

    this.currentAction = 'viewmap'
}

Fuel.getFuelEntryById = async function(fuelEntryId) {
    const queryString = new URLSearchParams({
        fuel_entry_id: fuelEntryId || 0
    }).toString()

    const url = `${this.endPoints.get('list')}?${queryString}`
    const serverResponse = await ( await _$.cfetch(url) ).json()

    if (serverResponse.length === 1) {
        return serverResponse[0]
    } throw `Fuel Entry not found with an ID of: ${fuelEntryId}`
}

Fuel.terminate = function() {

}

export default Fuel
