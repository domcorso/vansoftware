import _$ from 'lib/globalhelper'

const DataTable = {}

/**
 * @param  {HTMLTableElement} datatable
 * @param  {string[]} headers=[]
 */
DataTable.setHeaders = function(datatable, headers = []) {
    const tableHead = datatable.querySelector('thead tr')

    _$.emptyElem(tableHead)

    headers.forEach(header => {
        const tableHeader = _$.elm('th')
        tableHeader.textContent = header
        tableHead.appendChild(tableHeader)
    })
}

/**
 * @param  {HTMLTableElement} datatable
 * @param  {Array} data=[]
 * @param  {Function} onRowClick=function() {}
 */
DataTable.populate = function(datatable, data = [], onRowClick = function() {}, selectable = false) {
    const tableBody = datatable.querySelector('tbody')
    
    if (selectable) {
        datatable.classList.add('datatable--selectable')
    }

    _$.emptyElem(tableBody)
    
    data.forEach(row => {
        const tableRow = _$.elm('tr')

        if (selectable) {
            const td = _$.elm('td')
            const checkbox = _$.elm('div')

            td.classList.add('datatable__checkbox-container')
            checkbox.classList.add('datatable__checkbox')

            td.appendChild(checkbox)
            tableRow.appendChild(td)

            tableRow.addEventListener('click', function() {
                this.classList.toggle('datatable__row--selected')
            })

            if (row.initSelected) {
                tableRow.classList.add('datatable__row--selected')
            }
        }

        // Render table cells (<td>) for all display sells in this 'row'
        row.displayCells.forEach(column => {
            const tableData = _$.elm('td')
            tableData.textContent = column
            tableRow.appendChild(tableData)
        })
        
        // Add all data about this row to the dataset under 'allData'
        tableRow.dataset.allData = JSON.stringify(row.allData)

        tableRow.addEventListener('click', function(e) {
            onRowClick.call(this, e, row.allData)
        })

        tableBody.appendChild(tableRow)
    })

    this.resizeTableHeaders(datatable)
}

/**
 * Used for fixed table headers - will resize the <th>'s in the header
 * to match the widths of the <td>'s found in the <tbody>. A table with
 * a single header will just expand the entire width of the table.
 * @param  {HTMLTableElement} datatable
 */
DataTable.resizeTableHeaders = function(datatable) {
    const tableHeaders = datatable.querySelectorAll('thead th')
    const firstRowCells = datatable.querySelectorAll('tbody tr:first-of-type td')

    /* If there is no data, don't use a "fixed" datatable, this
    way it will re-align naturally */
    if (firstRowCells.length === 0) {
        datatable.classList.remove('datatable--fixed-header')
        return
    }
    
    /* Set this table as having to have a fixed header with scrollable content */
    datatable.classList.add('datatable--fixed-header')

    /* Allow the table header to have a single row which expands with 100% */
    if (tableHeaders.length === 1) {
        datatable.classList.add('datatable--single-header')
        return
    }
    
    /* This table has more than 1 header, so remove this class */
    datatable.classList.remove('datatable--single-header')

    tableHeaders.forEach((th, i) => {
        th.style.width = firstRowCells[i].scrollWidth + 'px'
    });
}

/**
 * Returns an array of <tr>'s in a given datatable
 * @param  {HTMLTableElement} datatable
 */
DataTable.getRows = function(datatable) {
    return Array.from(datatable.querySelectorAll('tbody tr'))
}

/**
 * Returns an array of <tr>'s that are currently selected. Only used for
 * "selectable" tables.
 * @param  {HTMLTableElement} datatable
 */
DataTable.getSelected = function(datatable) {
    return this.getRows.filter(row => {
        return this.isRowSelected(row)
    })
}

/**
 * Returns true/false depending on if a given row is selected
 * @param  {HTMLTableRowElement} row
 */
DataTable.isRowSelected = function(row) {
    return row.classList.contains('datatable__row--selected')
}

export default DataTable
