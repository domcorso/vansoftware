import _$ from 'lib/globalhelper'

const Alerts = {
    alertStyleList: new Map([
        ['success', {
            imgPath: 'images/alert-box/success.png',
            class: 'ab-status-success'
        }],
        ['error', {
            imgPath: 'images/alert-box/error.png',
            class: 'ab-status-error'
        }],
        ['warning', {
            imgPath: 'images/alert-box/warning.png',
            class: 'ab-status-warning'
        }],
        ['info', {
            imgPath: 'images/alert-box/info.png',
            class: 'ab-status-info'
        }]
    ]),

    buttonStyleList: new Map([
        ['ok', {
            buttonClass: 'alerts-button--ok',
            iconClass: 'ion-ios-checkmark'
        }],
        ['cancel', {
            buttonClass: 'alerts-button--cancel',
            iconClass: 'ion-ios-close'
        }],
        ['external', {
            buttonClass: '',
            iconClass: 'ion-md-open'
        }],
    ]),

    defaultAlert: {
        type: 'info',
        iconPath: '',
        title: '',
        message: '',
        buttons: [
            {
                type: 'ok',
                callback: function () {}
            }
        ]
    },

    e: {
        overlay: null,
        box: null,
        iconContainer: null,
        icon: null,
        messageContainer: null,
        messageTitle: null,
        messageContent: null,
        buttonContainer: null
    },

    queue: [],
    visible: false
}

Alerts.init = function() {
    this.addToDOM()
    return this
}

Alerts.addToDOM = function() {
    if (document.getElementById('alerts-overlay') !== null) {
        console.warn('Alert HTML already in DOM')
        return
    }

    this.e.overlay = _$.elm('div')
    this.e.box = _$.elm('div')
    this.e.iconContainer = _$.elm('div')
    this.e.icon = _$.elm('img')
    this.e.messageContainer = _$.elm('div')
    this.e.messageTitle = _$.elm('div')
    this.e.messageContent = _$.elm('div')
    this.e.buttonContainer = _$.elm('div')

    // Overlay
    {
        this.e.overlay.id = 'alerts-overlay'
        this.e.overlay.appendChild(this.e.box)
    }

    // Box
    {
        this.e.box.id = 'alerts-box'
        this.e.box.appendChild(this.e.iconContainer)
        this.e.box.appendChild(this.e.messageContainer)
    }
    
    // Icon Container
    {
        this.e.iconContainer.id = 'alerts-icon-container'
        this.e.iconContainer.appendChild(this.e.icon)
    }

    // Icon
    {
        this.e.icon.id = 'alerts-icon'
    }

    // Message Container
    {
        this.e.messageContainer.id = 'alerts-message-container'
        this.e.messageContainer.appendChild(this.e.messageTitle)
        this.e.messageContainer.appendChild(this.e.messageContent)
        this.e.messageContainer.appendChild(this.e.buttonContainer)
    }

    // Message Title
    {
        this.e.messageTitle.id = 'alerts-message-title'
    }

    // Message Content
    {
        this.e.messageContent.id = 'alerts-message-content'
    }

    // Message Button Container
    {
        this.e.buttonContainer.id = 'alerts-button-container'
    }

    this.setVisible(false)
    document.body.appendChild(this.e.overlay)
}

Alerts.push = function(options) {
    this.queue.push(options)
    if (this.visible === false) this.showRecent()
}

Alerts.showRecent = function() {
    if (this.queue.length === 0) {
        return
    }

    // Get most recent alert - normalize with default object
    const alert = Object.assign({}, this.defaultAlert, this.queue.shift())
    
    // Show alert
    this.showAlert(alert)
}

Alerts.showAlert = function(alert) {
    const alertStyles = this.alertStyleList.get(alert.type)

    // Apply alert styles
    this.e.box.className = alertStyles.class
    this.e.icon.src = alertStyles.imgPath

    // Add title
    this.e.messageTitle.textContent = alert.title

    // Add text
    if (alert.message instanceof Node) {
        alert.message = new XMLSerializer().serializeToString(alert.message)
    }
    this.e.messageContent.innerHTML = alert.message

    // Add buttons
    {
        _$.emptyElem(this.e.buttonContainer)
    
        for (const buttonOptions of alert.buttons) {
            const buttonStyles = this.buttonStyleList.get(buttonOptions.type)
            const button = this.createButton({
                buttonClass: buttonStyles.buttonClass,
                iconClass: buttonStyles.iconClass,
                callback: buttonOptions.callback,
                type: buttonOptions.type
            })
    
            this.e.buttonContainer.appendChild(button)
        }
    }

    this.setVisible(true)
}

Alerts.setVisible = function(b = true) {
    if (b) {
        this.e.overlay.style.display = ""
        this.visible = true
    } else {
        this.e.overlay.style.display = "none"
        this.visible = false
    }
}

Alerts.createButton = function(options) {
    const button = _$.elm('button')
    const icon = _$.elm('i')

    // Choose callback
    let callback = () => {}
    if (['cancel', 'ok'].includes(options.type) && !options.callback) {
        callback = () => {
            this.setVisible(false)
            this.showRecent()
        }
    } else {
        callback = () => {
            this.setVisible(false)
            options.callback()
        }
    }

    // Button class
    {
        button.classList.add('alerts-button')
        if (options.buttonClass.length) {
            button.classList.add(options.buttonClass)
        }
    }

    // Icon class
    {
        icon.classList.add('icon')
        if (options.iconClass.length) {
            icon.classList.add(options.iconClass)
        }
    }

    button.appendChild(icon)
    button.addEventListener('click', callback)

    return button
}

export default Alerts
