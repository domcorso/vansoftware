import _$ from 'lib/globalhelper'

const AppSwitcher = {
    e: {
        appContainer: null
    },

    appList: [],
    activeApp: null,
    defaultModule: {
        init() {
            console.warn('Default Module - initialized')
        },
        terminate() {
            console.warn('Default Module - terminated')
        }
    }
}

AppSwitcher.init = async function(modules = {}) {
    this.e.appContainer = _$.byId('app-container')

    // Retrieve application list
    try {
        this.appList = await ( await _$.cfetch('/api/retrieve__app-list.php') ).json()
    } catch (e) {
        window.app.fatalError('Could not load application list!', e)
    }

    // Resolve fields to useful items for each app
    for (const app of this.appList) {
        app.path = `templates/${app.id}.php`
        app.module = eval(`modules.${app.id}`) || this.defaultModule
    }

    // Load the first app by default - should be the App Drawer
    if (this.appList[0]) {
        this.open(this.appList[0].id)
    }

    return this
}

/**
 * @param {string} appId
 * @param {boolean} force - Force the app to load even if it is currently open
 */
AppSwitcher.open = async function(appId, force = false) {
    const app = this.getAppById(appId)

    // Don't load this app if it's currently open
    if (!force && this.activeApp === app) {
        console.warn(`App ${this.activeApp.title} is already open!`)
        return
    }

    // Attempt to retrieve the HTML for this app
    let html = ''
    try {
        const response = await _$.cfetch(app.path)

        if (!response.ok) {
            throw new Error('App HTML response not OK')
        }

        html = await response.text()
    } catch (e) {
        window.app.fatalError(`Could not open app "${appId}"`, e)
        return
    }

    this.e.appContainer.classList.remove('kf-app-in')
    this.e.appContainer.classList.add('kf-app-out')

    // Terminate the currently active application, if any
    if (this.activeApp && this.activeApp.module.terminate) {
        this.activeApp.module.terminate()
    }

    this.e.appContainer.addEventListener('animationend', () => {
        this.e.appContainer.classList.remove('kf-app-out')
        this.e.appContainer.classList.add('kf-app-in')

        // Show the HTML for this app
        _$.emptyElem(this.e.appContainer)
        this.e.appContainer.insertAdjacentHTML('afterbegin', html)

        // Initialize this app and set it as active
        this.e.appContainer.dataset.app = app.id
        app.module.init()
        this.activeApp = app
    }, { once: true })
}

AppSwitcher.getAppById = function (appId) {
    for (const app of this.appList) {
        if (app.id === appId) {
            return app
        }
    } return null
}

export default AppSwitcher
