import _$ from 'lib/globalhelper'

const PanelSwitcher = {
    CLASS_PANEL_ACTIVE: 'panel--active',
    CLASS_PANEL_NO_TRANSITION: 'panel--no-transition'
}

/**
 * Initializes the PanelSwitcher by adding events for all
 * elements with 'data-panel-link'. These ones, when clicked on
 * will open the panel they specify.
 */
PanelSwitcher.init = function() {
    document.addEventListener('click', e => {
        const dataset = e.target.dataset

        if (dataset.panelLink !== undefined) {
            this.showPanel(dataset.panelLink)
        }
    })
}

/**
 * Hides the current panel and show's a new panel, provided
 * as an argument.
 * @param {string} panelName
 */
PanelSwitcher.showPanel = function(panelName) {
    const panel = this.getPanelByName(panelName)
    const activePanels = _$.selAll(`.${this.CLASS_PANEL_ACTIVE}`)

    if (!panel) {
        console.warn('Cannot find panel with the name ' + panelName)
        return
    }

    activePanels.forEach(panel => panel.classList.remove(this.CLASS_PANEL_ACTIVE, this.CLASS_PANEL_NO_TRANSITION))
    panel.classList.add(this.CLASS_PANEL_ACTIVE)
    
    // If there are currently no active panels, do not use a transition
    if (activePanels.length === 0) {
        panel.classList.add(this.CLASS_PANEL_NO_TRANSITION)
    }
}

PanelSwitcher.getPanelByName = function(panelName) {
    return _$.sel(`[data-panel-name='${panelName}']`)
}

/**
 * @param  {string} panelName identifies the panel
 * @param  {string} title the title to set
 */
PanelSwitcher.setTitle = function(panelName, title) {
    const panel = this.getPanelByName(panelName)

    if (panel) {
        panel.querySelector('.panel__title').innerHTML = title
    }
}

export default PanelSwitcher
