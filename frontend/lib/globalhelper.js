const GlobalHelper = {}

/**
 * Creates a new element provided a tag name
 * @param {string} tagName='div'
 */
GlobalHelper.elm = function(tagName = 'div') {
    return document.createElement(tagName)
}

/**
 * Emptys an element by removing all child nodes
 * @param {HTMLElement} elem 
 */
GlobalHelper.emptyElem = function(elem) {
    while (elem.firstChild) {
        elem.removeChild(elem.firstChild)
    }
}

/**
 * Gets an element by id - shorthand
 * @param {string} id=''
 */
GlobalHelper.byId = function(id = '') {
    return document.getElementById(id)
}

/**
 * Gets an element by selector
 * @param {string} selector=''
 */
GlobalHelper.sel = function(selector = '') {
    return document.querySelector(selector)
}

/**
 * Gets all elements by selector
 * @param {string} selector=''
 */
GlobalHelper.selAll = function(selector = '') {
    return document.querySelectorAll(selector)
}
/**
 * Adds a click listener to each element that matches the selector
 * with the provided callback function
 * @param  {} selector=''
 * @param  {} callback=function() {}
 */
GlobalHelper.click = function(selector = '', callback = function() {}) {
    this.selAll(selector).forEach(elm => {
        elm.addEventListener('click', callback)
    })
}

/**
 * Converts an array of strings to an HTML list
 * @param {string[]} array=[]
 * @param {Boolean} ordered=false Set this to true to create an <ol> over an <ul>
 */
GlobalHelper.arrayToHtmlList = function(array = [], ordered = false) {
    const parentElem = this.elm(ordered ? 'ol' : 'ul')

    for (const itemText of array) {
        const li = this.elm('li')
        li.textContent = itemText
        parentElem.appendChild(li)
    }

    return parentElem
}
/**
 * Sets the value to '' for input fields
 * @param {(HTMLElement[]|HTMLElement)} elements
 */
GlobalHelper.clearInputFields = function(elements) {
    const toClear = elements instanceof Array ? elements : [elements]

    for (const element of toClear) {
        if (element instanceof HTMLInputElement || element instanceof HTMLTextAreaElement) {
            element.value = ''
        }
    }
}
/**
 * A wrapper around the fetch() method that includes
 * credentials: 'same-origin' by default, with extra options
 * being passed in through the init parameter
 * @param  {} input
 * @param  {} init
 */
GlobalHelper.cfetch = function(input, init) {
    init = Object.assign({}, { credentials: 'same-origin' }, init)
    return fetch(input, init)
}

module.exports = GlobalHelper
