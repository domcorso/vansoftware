<?php

require_once realpath(__DIR__ . "/../../app/Bootstrap.php");

$result = $app->logout();

header("Location: /mobile/login");
die("You are now logged out.");
