<?php

require_once realpath(__DIR__ . "/../../app/Bootstrap.php");

if ($app->getCurrentUser() !== null) {
    header("Location: /mobile");
    die("You are already logged in.");
}

$repo = UserRepository::getInstance();
$users = $repo->findAll();

$attemptedLogin = isset($_POST["vs-user-id"]) && isset($_POST["vs-password"]);
$loginSuccessful = false;

if ($attemptedLogin) {
    $userId = (int) trim($_POST["vs-user-id"]);
    $password = trim($_POST["vs-password"]);

    $loginSuccessful = $app->loginAsUser($userId, $password);

    if ($loginSuccessful) {
        header("Location: /mobile");
        die("You are now logged in.");
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="/mobile/css/login.css">
        <title>Login - Van Software</title>
    </head>
    <body>
        <div class="login-container">
            <img class="login-container__logo" src="/images/unlucky-logo.png" href="Van Software">
            <?php if ($attemptedLogin && !$loginSuccessful) { ?>
                <div class="form-messages form-messages--error">
                    <ul class="form-messages__list">
                        <li class="form-messages__item">The password you entered is incorrect</li>
                    </ul>
                </div>
            <?php } ?>
            <form action="./" method="post" autocomplete="off">
                <select class="form-select" name=vs-user-id id="vs-user-id">
                    <option value="0">Select User</option>
                    <?php foreach ($users as $user) { ?>
                        <option value="<?= $user->getId() ?>"><?= htmlspecialchars($user->getDisplayName()) ?></option>
                    <?php } ?>
                </select>
                <input type="password" class="form-input" name="vs-password" id="vs-password" placeholder="Password">
                <button type="submit" class="form-button form-button--primary form-button--full-width">Login</button>
            </form>
        </div>
    </body>
</html>
