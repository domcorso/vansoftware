<?php

require_once realpath(__DIR__ . "/../../app/Bootstrap.php");

if ($app->getCurrentUser() === null) {
    header("Location: /mobile/login");
    die("You need to be logged in to view this page.");
}

if (!$app->permissions->canCreateOrUpdateFuelEntries()) {
    require_once(realpath(PUB_MOBILE . "/unauthorized.php"));
    http_response_code(401);
    die();
}

loadModel("FuelEntry");

$fuelEntryRepo = FuelEntryRepository::getInstance();
$tripRepo = TripRepository::getInstance();

$urlParamId = (int) isset($_GET["fuel_entry_id"]) ? $_GET["fuel_entry_id"] : 0;
$fuelEntry = $fuelEntryRepo->getFuelEntryById($urlParamId);

if ($fuelEntry === null) {
    require_once(realpath(PUB_MOBILE . "/notfound.php"));
    http_response_code(404);
    die();
}

$formMessages = [];
$formSubmitted = isset($_POST["fuel_entry_save"]);

if ($formSubmitted) {
    $fuelEntry->setLitres($_POST["litres"]);
    $fuelEntry->setCostPerLitre($_POST["cost_per_litre"]);
    $fuelEntry->setTotalCost($_POST["total_cost"]);
    $fuelEntry->setPhotoFile($_POST["receipt_photo_filename"]);

    $validation = $fuelEntry->validate();
    $formMessages = $validation["messages"];
    $saved = $fuelEntryRepo->save($fuelEntry) !== false;

    if ($saved) {
        $app->setSessionMessage("Fuel Entry saved.", "success");
    }
}

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once realpath(PUB_MOBILE . "/include/meta-css-lib.php"); ?>
        <script src="/mobile/js/fuel-form.js"></script>
        <title>Edit Fuel Entry - Van Software</title>
    </head>
    <body>
        <?php require_once realpath(PUB_MOBILE . "/include/header.php"); ?>
        <?php require_once realpath(PUB_MOBILE . "/include/navigation.php"); ?>
        <div id="main-content">
            <?php require_once realpath(PUB_MOBILE . "/include/session-message.php"); ?>
            <div class="title">Edit Fuel Entry</div>
            <?php require_once realpath(__DIR__ . "/include/fuel-form.php"); ?>
        </div>
    </body>
</html>
