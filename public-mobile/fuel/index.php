<?php

require_once realpath(__DIR__ . "/../../app/Bootstrap.php");

if ($app->getCurrentUser() === null) {
    header("Location: /mobile/login");
    die("You need to be logged in to view this page.");
}

loadModel("FuelEntry");

$fuelEntries = FuelEntryRepository::getInstance()->findAll();

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once realpath(PUB_MOBILE . "/include/meta-css-lib.php"); ?>
        <title>Fuel Tracking - Van Software</title>
    </head>
    <body>
        <?php require_once realpath(PUB_MOBILE . "/include/header.php"); ?>
        <?php require_once realpath(PUB_MOBILE . "/include/navigation.php"); ?>
        <div id="main-content">
            <?php require_once realpath(PUB_MOBILE . "/include/session-message.php"); ?>
            <div class="title">Fuel Tracking</div>
            <div class="button-ribbon">
                <a href="/mobile/fuel/create.php" class="button-ribbon__button">New Entry</a>
            </div>
            <div class="table__header">Fuel History</div>
            <table class="table">
                <tbody>
                    <?php foreach ($fuelEntries as $fuelEntry) { ?>
                        <tr class="table__row" data-href="/mobile/fuel/edit.php?fuel_entry_id=<?= $fuelEntry->getId() ?>">
                            <td class="table__cell"><?= $fuelEntry->getCreatedAt()->format("D jS M Y") ?></td>
                            <td class="table__cell table__cell--right">$<?= number_format($fuelEntry->getTotalCost(), 2) ?></td>
                        </tr>
                    <?php } ?>
                    <?php if (count($fuelEntries) === 0) { ?>
                        <tr class="table__row">
                            <td class="table__cell table__cell--singular">No history found.</td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </body>
</html>
