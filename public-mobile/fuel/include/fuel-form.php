<?php if (count($formMessages) > 0) { ?>
    <div class="form-messages form-messages--error">
        <ul class="form-messages__list">
            <?php foreach ($formMessages as $message) { ?>
                <li class="form-messages__item"><?= $message ?></li>
            <?php } ?>
        </ul>
    </div>
<?php } ?>

<form method="post">
    <label class="form-label" for="litres">Litres</label>
    <input type="text" class="form-input" name="litres" id="inpLitres" value="<?= $fuelEntry->getLitres() ?>" placeholder="0.00">

    <label class="form-label" for="cost_per_litre">Cost Per Litre ($)</label>
    <input type="text" class="form-input" name="cost_per_litre" id="inpCostPerLitre" value="<?= $fuelEntry->getCostPerLitre() ?>" placeholder="0.000">

    <label class="form-label" for="total_cost">Total Cost ($)</label>
    <input type="text" class="form-input" name="total_cost" id="inpTotalCost" value="<?= $fuelEntry->getTotalCost() ?>" placeholder="0.00">

    <div class="form-divider"></div>
    <div class="form-button-ribbon">
        <button type="button" class="form-button" id="btnCaptureReceipt">Capture Receipt</button>
    </div>

    <!-- Receipt Photo Upload -->
    <img src="<?= $fuelEntry->getPhotoUrl() ?>" id="imgReceiptPhoto" class="image <?= empty($fuelEntry->getPhotoFile()) ? 'hidden' : '' ?>" alt="<?= $fuelEntry->getPhotoFile() ?>">
    <input type="hidden" name="receipt_photo_filename" id="inpReceiptPhotoFilename" value="<?= $fuelEntry->getPhotoFile() ?>">

    <input type="hidden" name="fuel_entry_id" value="<?= $fuelEntry->getId() ?>">
    <button type="submit" class="form-button form-button--primary form-button--full-width" name="fuel_entry_save">Save</button>
</form>
