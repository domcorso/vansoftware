<?php $sessionMessage = $app->getSessionMessage(); ?>

<?php if ($sessionMessage !== null) { ?>
    <?php $htmlModifierClass = $sessionMessage["type"] === "success" ? "form-messages--success" : "form-messages--error"; ?>
    <div class="form-messages <?= $htmlModifierClass ?>">
        <span class="form-messages__item"><?= $sessionMessage["message"] ?></span>
    </div>
<?php } ?>
