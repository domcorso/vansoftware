<div class="header">
    <button id="btn-open-nav" class="header__btn" type="button">
        <i class="icon ion-md-menu"></i>
    </button>
    <button id="btn-user-settings" class="header__btn" type="button">
        <i class="icon ion-md-settings"></i>
    </button>
</div>
