<?php require_once realpath(__DIR__ . "/../../app/Bootstrap.php");?>

<div id="navigation" class="navigation">
    <div class="navigation__display-name">
        <?= $app->getCurrentUser()->getDisplayName(); ?>
    </div>
    <div class="navigation__buttons-container">
        <a href="/mobile" class="navigation__button">
            <div class="navigation__button-text">Home</div>
            <i class="navigation__button-icon icon ion-md-home"></i>
        </a>

        <?php if ($app->permissions->canListFuelEntries()) { ?>
            <a href="/mobile/fuel" class="navigation__button">
                <div class="navigation__button-text">Fuel Tracking</div>
                <i class="navigation__button-icon icon ion-md-flame"></i>
            </a>
        <?php } ?>

        <a href="/mobile/login/logout.php" class="navigation__button">
            <div class="navigation__button-text">Logout</div>
            <i class="navigation__button-icon icon ion-md-log-out"></i>
        </a>
    </div>
</div>
<div id="navigation-overlay" class="navigation__overlay"></div>
