<?php

require_once realpath(__DIR__ . "/../app/Bootstrap.php");

if ($app->getCurrentUser() === null) {
    header("Location: /mobile/login");
    die("You need to be logged in to view this page.");
}

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once realpath(PUB_MOBILE . "/include/meta-css-lib.php"); ?>
        <title>Not Found - Van Software</title>
    </head>
    <body>
        <?php require_once realpath(PUB_MOBILE . "/include/header.php"); ?>
        <?php require_once realpath(PUB_MOBILE . "/include/navigation.php"); ?>
        <div id="main-content">
            <?php require_once realpath(PUB_MOBILE . "/include/session-message.php"); ?>
            <div class="title">404 Not Found</div>
            <p>
                Sorry, but that resource could not be found
            </p>
        </div>
    </body>
</html>
