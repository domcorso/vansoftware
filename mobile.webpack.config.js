const webpack = require('webpack')
const path = require('path')

module.exports = {
    entry: {
        'mobile-main': './frontend/mobile/main.js',
        'fuel-form': './frontend/mobile/fuel/fuel-form.js'
    },
    output: {
        filename: '[name].js',
        path: path.resolve('public-mobile', 'js')
    },
    watch: process.argv.includes('--watch'),
    resolve: {
        alias: {
            lib: path.resolve(__dirname, 'frontend/lib')
        }
    }
}
