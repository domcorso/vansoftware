-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               10.1.36-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5277
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for vansoftware
CREATE DATABASE IF NOT EXISTS `vansoftware` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `vansoftware`;

-- Dumping structure for table vansoftware.fuel_entry
CREATE TABLE IF NOT EXISTS `fuel_entry` (
  `fuel_entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `litres` decimal(7,2) unsigned NOT NULL DEFAULT '0.00',
  `cost_per_litre` decimal(7,3) unsigned NOT NULL DEFAULT '0.000',
  `total_cost` decimal(7,2) unsigned NOT NULL DEFAULT '0.00',
  `lat` decimal(9,6) DEFAULT NULL,
  `lng` decimal(9,6) DEFAULT NULL,
  `trip_id` int(10) unsigned NOT NULL DEFAULT '0',
  `entered_by` int(10) unsigned DEFAULT NULL,
  `photo_file` varchar(100) NOT NULL DEFAULT '',
  `created_utc` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`fuel_entry_id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table vansoftware.trip
CREATE TABLE IF NOT EXISTS `trip` (
  `trip_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `trip_name` varchar(45) NOT NULL,
  `trip_desc` varchar(500) DEFAULT NULL,
  `start_date` int(11) NOT NULL,
  `end_date` int(11) DEFAULT NULL,
  `archived` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`trip_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table vansoftware.trip_user
CREATE TABLE IF NOT EXISTS `trip_user` (
  `trip_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`trip_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table vansoftware.user
CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `display_name` varchar(20) NOT NULL,
  `password` char(64) NOT NULL,
  `color` char(7) DEFAULT NULL,
  `archived` bit(1) NOT NULL DEFAULT b'0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
