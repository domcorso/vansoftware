const webpack = require('webpack')
const path = require('path')

module.exports = {
    entry: './frontend/tablet/main.js',
    output: {
        filename: 'main.bundle.js',
        path: path.resolve('public-tablet', 'js')
    },
    watch: process.argv.includes('--watch'),
    resolve: {
        alias: {
            lib: path.resolve(__dirname, 'frontend/lib')
        }
    }
}
