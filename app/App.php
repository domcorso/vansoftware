<?php

require_once ROOT . "/Permissions.php";
loadModel("User");

class App {
    public $permissions = null;

    private $currentUser = null;
    private $loggedInAsTablet = false;
    private $gpsLocation = [];

    private $repos = null;

    private $env = [];

    public function __construct()
    {
        $this->parseEnvFile();

        $this->repos = new stdClass();
        $this->repos->user = UserRepository::getInstance();
        $this->repos->trip = TripRepository::getInstance();

        $this->permissions = Permissions::getInstance($this);
        $this->determineLoginInfo();
    }

    private function determineLoginInfo()
    {
        $this->loggedInAsTablet = isset($_SESSION["logged_in_as_tablet"]) && $_SESSION["logged_in_as_tablet"] === true;
        $this->currentUser = null;

        if (!$this->loggedInAsTablet && isset($_SESSION["user_id"])) {
            $user = $this->repos->user->getUserById($_SESSION["user_id"]);
            
            if ($user === null) {
                $this->logout();
            } else {
                $this->currentUser = $user;
            }
        }
    }
    
    private function parseEnvFile()
    {
        $envFilePath = realpath(ROOT . "/../.env");
        $envFile = fopen($envFilePath, "r");

        while (($line = fgets($envFile)) !== false) {
            if (strpos($line, "=") !== false) {
                $split = explode("=", $line);
                $key = trim($split[0]);
                $value = trim($split[1]);

                $this->env[$key] = $value;
            }
        }
    }

    private function getPinCode()
    {
        return trim(file_get_contents(RESOURCES . "/login/pincode"));
    }

    public function getInstance() : App
    {
        static $instance = null;

        if ($instance === null) {
            $instance = new App();
        } return $instance;
    }

    public function isLoggedIn() : bool
    {
        return $this->loggedInAsTablet || $this->currentUser !== null;
    }

    public function loggedInAsTablet() : bool
    {
        return $this->loggedInAsTablet;
    }

    public function getCurrentUser()
    {
        return $this->currentUser;
    }

    public function loginAsTablet(string $pinCode) : bool
    {
        if (!$this->loggedInAsTablet || $this->currentUser === null) {
            if ($pinCode === $this->getPinCode()) {
                $this->logout();
                $_SESSION["logged_in_as_tablet"] = true;
                $this->determineLoginInfo();

                return true;
            }
        } return false;
    }

    public function loginAsUser(int $userId, string $password) : bool
    {
        $user = $this->repos->user->getUserById($userId);

        if ($user !== null && $user->getPassword() === User::hashPassword($password)) {
            $this->logout();
            $_SESSION["user_id"] = $user->getId();
            $this->determineLoginInfo();
            return true;
        } return false;
    }

    public function logout() : bool
    {
        if ($this->loggedInAsTablet || $this->currentUser !== null) {
            session_destroy();
            $_SESSION = [];
            $this->determineLoginInfo();

            return true;
        } return false;
    }

    public function setSessionMessage(string $message, string $type)
    {
        $_SESSION["session_message"] = $message;
        $_SESSION["session_message_type"] = $type;
    }

    public function getSessionMessage()
    {
        if (isset($_SESSION["session_message"]) && strlen($_SESSION["session_message"]) > 0) {
            $message = $_SESSION["session_message"];
            $type = $_SESSION["session_message_type"];

            $_SESSION["session_message"] = "";
            $_SESSION["session_message_type"] = "";

            return [
                "message" => $message,
                "type" => $type
            ];
        } return null;
    }

    public function getGpsLocation() : array
    {
        if (empty($this->gpsLocation)) {
            $command = "powershell.exe " . realpath(SHELL . "/gps_location.ps1");
            $output = shell_exec($command);
            $outputPattern = "/lat:([-\d\.]+),lng:([-\d\.]+)/";
            $regexMatches = [];
    
            if ($output === null || preg_match($outputPattern, $output, $regexMatches) === 0) {
                return [
                    "latitude" => 0,
                    "longitude" => 0
                ];
            }

            $this->gpsLocation = [
                "latitude" => (float) $regexMatches[1],
                "longitude" => (float) $regexMatches[2]
            ];
        }
        
        return $this->gpsLocation;
    }

    public function getEnv() : array
    {
        return $this->env;
    }

    public function env($key) : string
    {
        if (array_key_exists($key, $this->env)) {
            return $this->env[$key];
        } return "";
    }
}
