<?php

class Database {
    private $dbConn;

    private function __construct () {
        $dsn = "mysql:dbname=vansoftware;host:127.0.0.1";
        $username = "root";
        $password = "";

        # Connect to database using a PDO
        # Sets the fetch mode to "FETCH_ASSOC" so the table columns are returned
        # from a statement rather than both table columns AND numeric array indexes
        try {
            $this->dbConn = new PDO($dsn, $username, $password);
            $this->dbConn->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            die("Database connection failed: " . $e->getMessage());
        }
    }

    public static function getInstance () {
        static $instance = null;

        if ($instance === null) {
            $instance = new self();
        } return $instance;
    }

    public function getConn () {
        return $this->dbConn;
    }
}
