<?php

abstract class AbstractModel implements JsonSerializable {
    protected static function normalizeFields (array $data, array $allFields) {
        foreach ($allFields as $key) {
            $data[$key] = isset($data[$key]) ? $data[$key] : null;
            $data[$key] = $data[$key] === "NULL" ? null : $data[$key];
        }

        return $data;
    }

    protected static function dateTimeFromTS ($timestamp) {
        if (is_numeric($timestamp)) {
            $dateTime = new DateTime();
            $dateTime->setTimestamp((int) $timestamp);

            return $dateTime;
        } return null;
    }

    protected function validateRequiredFields () : array {
        $messages = [];

        foreach (static::REQUIRED_FIELDS as $requiredField) {
            $propName = $requiredField[0];
            $displayName = $requiredField[1];

            if (AbstractModel::isPropertyEmpty($propName)) {
                $messages[] = $displayName . " is required";
            }
        }

        return $messages;
    }

    protected function isPropertyEmpty ($propName) : bool {
        return is_null($this->$propName) || (is_string($this->$propName) && strlen($this->$propName) === 0);
    }

    public function jsonSerialize () : array {
        return get_object_vars($this);
    }
}
