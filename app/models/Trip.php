<?php

class Trip extends AbstractModel {
    const REQUIRED_FIELDS = [
        ["name", "Name"],
        ["startDate", "Start Date"]
    ];

    const NAME_MAX_LEN = 45;
    const DESC_MAX_LEN = 500;

    protected $id = null;
    protected $name = null;
    protected $description = null;
    protected $startDate = null;
    protected $endDate = null;
    protected $archived = false;

    public static function fromDataSource ($data) {
        $instance = new self();

        $data = parent::normalizeFields($data, [
            "trip_id",
            "trip_name",
            "trip_desc",
            "start_date",
            "end_date",
            "archived"
        ]);

        $instance->id = empty($data["trip_id"]) ? null : $data["trip_id"];
        $instance->name = $data["trip_name"];
        $instance->description = $data["trip_desc"];
        $instance->startDate = parent::dateTimeFromTS($data["start_date"]);
        $instance->endDate = parent::dateTimeFromTS($data["end_date"]);
        $instance->archived = !is_null($data["archived"]);

        return $instance;
    }

    public function __construct () {

    }

    public function getId () {
        return $this->id;
    }

    public function getName () {
        return $this->name;
    }

    public function getDescription () {
        return $this->description;
    }

    public function getStartDate () {
        return $this->startDate;
    }

    public function getEndDate () {
        return $this->endDate;
    }

    public function isArchived () {
        return $this->archived;
    }

    public function setName (string $name) : Trip {
        $this->name = $name;
        return $this;
    }

    public function setDescription (string $description) : Trip {
        $this->description = $description;
        return $this;
    }

    public function setStartDate (DateTime $dateTime) : Trip {
        $this->startDate = $dateTime;
        return $this;
    }

    public function setEndDate (DateTime $dateTime) : Trip {
        $this->endDate = $dateTime;
        return $this;
    }

    public function setArchived (bool $archived) : Trip {
        $this->archived = $archived;
        return $this;
    }

    public function validate () : array {
        $messages = $this->validateRequiredFields();

        if (strlen($this->name) > self::NAME_MAX_LEN) {
            $messages[] = "Name is too long (max length: " . self::NAME_MAX_LEN . " characters)";
        }

        if (strlen($this->description) > self::DESC_MAX_LEN) {
            $messages[] = "Description is too long (max length: " . self::DESC_MAX_LEN . " characters)";
        }

        if ($this->startDate && $this->endDate) {
            if ($this->endDate < $this->startDate) {
                $messages[] = "End Date must be past the Start Date";
            }
        }

        return [
            "messages" => $messages,
            "valid" => count($messages) === 0
        ];
    }
}
