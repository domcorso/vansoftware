<?php

require_once REPOSITORIES . "/UserRepository.php";

class User extends AbstractModel {
    const REQUIRED_FIELDS = [
        ["displayName", "Display Name"],
        ["password", "Password"]
    ];

    const DISPLAY_NAME_MAX_LEN = 20;
    const PASSWORD_MIN_LEN = 8;
    const PROFILE_PHOTO_DIR = DATA . "/profile-photos";
    const DEFAULT_PROFILE_PHOTO = RESOURCES . "/users/default-profile-photo.jpg";
    const COLOR_PATTERN = "/^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/";
    const DEFAULT_COLOR = "#3399ff";

    protected $id = null;
    protected $displayName = null;
    protected $password = null;
    protected $color = null;
    protected $isAdmin = true; # Temporary until a new field for isAdmin is added to users
    protected $createdAt = null;
    protected $archived = false;

    # Used for validation when a new user has been created or a
    # password is being updated
    protected $plainTextPassword = null;

    # Keep track of the original display name from the database
    # to avoid a validation issue when saving existing users
    protected $originalDisplayName = null;

    public static function fromDataSource (array $data) : User {
        $instance = new User();

        $data = parent::normalizeFields($data, [
            "user_id",
            "display_name",
            "password",
            "color",
            "archived"
        ]);

        $instance->id = (int) $data["user_id"];
        $instance->displayName = $data["display_name"];
        $instance->password = strtolower($data["password"]);
        $instance->color = $data["color"];
        $instance->createdAt = parent::dateTimeFromTS($data["created_at"]);
        $instance->archived = (bool) $data["archived"];

        $instance->originalDisplayName = $data["display_name"];

        return $instance;
    }

    public static function hashPassword (string $password) : string {
        return strtolower(hash("sha256", $password));
    }

    public static function getProfilePhotoId (int $userId) : string {
        return hash("crc32", $userId);
    }

    public static function defaultColors () : array {
        return json_decode(
            file_get_contents(RESOURCES . "/users/default-color-list.json")
        );
    }

    public function __construct () {

    }

    public function getId () {
        return $this->id;
    }

    public function getDisplayName () : string {
        return $this->displayName;
    }

    public function getPassword () : string {
        return $this->password;
    } 

    public function getColor () : string {
        if ($this->color === null) {
            return User::DEFAULT_COLOR;
        } return $this->color;
    }

    public function isAdmin () : bool {
        return $this->isAdmin;
    }

    public function getProfilePhoto () : string {
        $filename = User::getProfilePhotoId($this->id) . ".jpg";
        $path = User::PROFILE_PHOTO_DIR . "/" . $filename;

        if ($this->id === null || !file_exists($path)) {
            $path = User::DEFAULT_PROFILE_PHOTO;
        }

        $imageData = file_get_contents($path);
        $base64 = base64_encode($imageData);

        return "data:image/jpeg;base64," . $base64;
    }

    public function isArchived () : bool {
        if ($this->archived === true) {
            return true;
        } return false;
    }

    private function setId (int $id) : User {
        $this->id = $id;
        return $this;
    }

    public function setDisplayName (string $displayName) : User {
        $this->displayName = $displayName;
        return $this;
    }

    private function setPassword (string $password) : User {
        $this->password = $password;
        return $this;
    }

    public function setPlainTextPassword (string $password) : User {
        $this->plainTextPassword = $password;
        $this->password = User::hashPassword($password);
        return $this;
    }

    public function setColor (string $color) : User {
        $this->color = $color;
        return $this;
    }

    public function setArchived (bool $archived) : User {
        $this->archived = $archived;
        return $this;
    }

    public function validate () : array {
        $messages = $this->validateRequiredFields();

        $userRepository = new UserRepository();
        $displayNameIsUsed = $userRepository->isDisplayNameUsed($this->displayName);
        $displayNameAltered = $this->originalDisplayName !== null && $this->originalDisplayName !== $this->displayName;

        if (($this->originalDisplayName === null || $displayNameAltered) && $displayNameIsUsed) {
            $messages[] = "Sorry, that Display Name is already in use!";
        }

        if (strlen($this->displayName) > User::DISPLAY_NAME_MAX_LEN) {
            $messages[] = "Display Name is too long (max length: " . User::DISPLAY_NAME_MAX_LEN . " characters)";
        }

        # Extra conditions if setting new password or creating a new user
        if ($this->plainTextPassword !== null || $this->id === null) {
            if (strlen($this->plainTextPassword) < User::PASSWORD_MIN_LEN) {
                $messages[] = "Password is too short (min length: " . User::PASSWORD_MIN_LEN . " characters)";
            }
        }

        if ($this->color !== null && preg_match(User::COLOR_PATTERN, $this->color) !== 1) {
            $messages[] = "That is not a valid colour, you should probably see Dom about this one";
        }

        return [
            "messages" => $messages,
            "valid" => count($messages) === 0
        ];
    }

    public function jsonSerialize() : array {
        $toSerialize = parent::jsonSerialize();

        $toSerialize["color"] = $this->getColor();
        $toSerialize["archived"] = $this->isArchived();
        $toSerialize["profilePhoto"] = $this->getProfilePhoto();

        # The returned data from this method is likely to be seen on the
        # client side so we should not be showing any passwords
        unset($toSerialize["plainTextPassword"], $toSerialize["password"]);

        return $toSerialize;
    }
}
