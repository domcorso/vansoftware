<?php

require_once REPOSITORIES . "/UserRepository.php";
require_once REPOSITORIES . "/TripRepository.php";

class FuelEntry extends AbstractModel
{
    const REQUIRED_FIELDS = [
        ["litres", "Litres"],
        ["costPerLitre", "Cost Per Litre"],
        ["totalCost", "Total Cost"],
        ["trip", "Trip"],
        ["enteredBy", "User"]
    ];

    protected $id = 0;
    protected $litres = null;
    protected $costPerLitre = null;
    protected $totalCost = null;
    protected $latitude = null;
    protected $longitude = null;
    protected $trip = null;
    protected $enteredBy = null;
    protected $photoFile = null;
    protected $createdAt = null;

    private $tripRepo = null;
    private $userRepo = null;

    public function __construct()
    {
        $this->tripRepo = TripRepository::getInstance();
        $this->userRepo = UserRepository::getInstance();
    }

    public static function fromDataSource (array $data) : FuelEntry
    {
        $instance = new FuelEntry();

        $data = parent::normalizeFields($data, [
            "fuel_entry_id",
            "litres",
            "cost_per_litre",
            "total_cost",
            "lat",
            "lng",
            "trip_id",
            "entered_by",
            "photo_file",
            "created_utc"
        ]);

        # Used to resolve foreign keys in data source to models
        $userRepo = UserRepository::getInstance();
        $tripRepo = TripRepository::getInstance();

        $instance->id = (int) $data["fuel_entry_id"];
        $instance->litres = (float) $data["litres"];
        $instance->costPerLitre = (float) $data["cost_per_litre"];
        $instance->totalCost = (float) $data["total_cost"];
        $instance->latitude = $data["lat"] === null ? null : (float) $data["lat"];
        $instance->longitude = $data["lng"] === null ? null : (float) $data["lng"];
        $instance->trip = $tripRepo->getTripById($data["trip_id"] === null ? 0 : $data["trip_id"]);
        $instance->enteredBy = $userRepo->getUserById($data["entered_by"] === null ? 0 : $data["entered_by"]);
        $instance->photoFile = $data["photo_file"];
        $instance->createdAt = DateTime::createFromFormat(DATETIME_FORMAT, $data["created_utc"]);

        return $instance;
    }

    public static function getReceiptPhotoPublicPrefix () : string
    {
        return "/uploads/fuel-entries";
    }

    public static function getReceiptPhotoDirectory () : string
    {
        return realpath(PUB_TABLET . "/uploads/fuel-entries");
    }

    public function getId ()
    {
        return $this->id;
    }

    public function getLitres()
    {
        return $this->litres;
    }

    public function getCostPerLitre()
    {
        return $this->costPerLitre;
    }

    public function getTotalCost()
    {
        return $this->totalCost;
    }

    public function getLatitude()
    {
        return $this->latitude;
    }

    public function getLongitude()
    {
        return $this->longitude;
    }

    public function getTrip()
    {
        return $this->trip;
    }

    public function getEnteredBy()
    {
        return $this->enteredBy;
    }

    public function getPhotoFile()
    {
        return $this->photoFile;
    }

    public function getPhotoUrl()
    {
        if ($this->getPhotoFile() !== "") {
            return FuelEntry::getReceiptPhotoPublicPrefix() . "/" . $this->getPhotoFile();
        } return "";
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setLitres ($litres) : FuelEntry
    {
        $this->litres = $litres;
        return $this;
    }

    public function setCostPerLitre ($costPerLitre) : FuelEntry
    {
        $this->costPerLitre = $costPerLitre;
        return $this;
    }
    
    public function setTotalCost ($totalCost) : FuelEntry
    {
        $this->totalCost = $totalCost;
        return $this;
    }

    public function setLatitude ($latitude) : FuelEntry
    {
        $this->latitude = $latitude;
        return $this;
    }

    public function setLongitude ($longitude) : FuelEntry
    {
        $this->longitude = $longitude;
        return $this;
    }

    public function setTripId ($tripId) : FuelEntry
    {
        $this->trip = $this->tripRepo->getTripById($tripId);
        return $this;
    }

    public function setEnteredById ($userId) : FuelEntry
    {
        $this->enteredBy = $this->userRepo->getUserById($userId);
        return $this;
    }

    public function setPhotoFile ($file) : FuelEntry
    {
        $this->photoFile = $file;
        return $this;
    }

    public function validate()
    {
        $latLngPattern = "/^\-?\d+\.\d+?$/";
        $messages = $this->validateRequiredFields();

        if (!$this->isPropertyEmpty("litres") && !is_numeric($this->litres)) {
            $messages[] = "Litres must be numeric";
        }

        if (!$this->isPropertyEmpty("costPerLitre") && !is_numeric($this->costPerLitre)) {
            $messages[] = "Cost Per Litre must be numeric";
        }

        if (!$this->isPropertyEmpty("totalCost") && !is_numeric($this->totalCost)) {
            $messages[] = "Total Cost must be numeric";
        }

        if ($this->latitude > 0 && preg_match($latLngPattern, $this->latitude) !== 1) {
            $messages[] = "Latitude is invalid";
        }

        if ($this->longitude > 0 && preg_match($latLngPattern, $this->longitude) !== 1) {
            $messages[] = "Longitude is invalid";
        }

        return [
            "messages" => $messages,
            "valid" => count($messages) === 0
        ];
    }

    public function jsonSerialize() : array {
        $toSerialize = parent::jsonSerialize();
        $toSerialize["receiptPhotoUrl"] = $this->getPhotoUrl();

        return $toSerialize;
    }
}
