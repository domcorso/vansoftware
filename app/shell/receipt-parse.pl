my $scannedText = $ARGV[0];
my $output = '';
my %results = (
    'costPerLitre' => 0,
    'totalFuel' => 0,
    'totalCost' => 0
);

# Find total cost
my $regex_tc = qr/(\$\d{2,3}\ ?\.\ ?\d{2})[^\d]/;
if ($scannedText =~ /$regex_tc/) {
    $results{'totalCost'} = $1;
    $results{'totalCost'} =~ s/\s//;
    $results{'totalCost'} =~ s/\$//;

    $scannedText =~ s/$regex_tc//g;
}

# Find cost per litre
my $regex_cpl = qr/[^\d]([12]\ ?\.\ ?\d{3})/;
if ($scannedText =~ /$regex_cpl/) {
    $results{'costPerLitre'} = $1;
    $results{'costPerLitre'} =~ s/\s//;

    $scannedText =~ s/$regex_cpl//g;
}

# Find total fuel
my $regex_tf = qr/[^\d](\d{2}\.\d{2})[^\d]/;
my $regex_tf2 = qr/(\d{2}\.\d{2})\ ?L/;
if ($scannedText =~ /$regex_tf/) {
    $results{'totalFuel'} = $1;
    $results{'totalFuel'} =~ s/\s//;
    
    $scannedText =~ s/$regex_tf//g;
}
elsif ($scannedText =~ /$regex_tf2/) {
    $results{'totalFuel'} = $1;
    $results{'totalFuel'} =~ s/\s//;
    
    $scannedText =~ s/$regex_tf2//g;
}

# Back-calculate "totalFuel" if necessary
if ($results{'costPerLitre'} ne 0 and $results{'totalCost'} ne 0 and $results{'totalFuel'} eq 0) {
    $results{'totalFuel'} = $results{'totalCost'} / $results{'costPerLitre'};
    $results{'totalFuel'} = sprintf('%.2f', $results{'totalFuel'});
}

# Back-calculate "totalCost" if necessary
if ($results{'costPerLitre'} ne 0 and $results{'totalFuel'} ne 0 and $results{'totalCost'} eq 0) {
    $results{'totalCost'} = $results{'totalFuel'} * $results{'costPerLitre'};
    $results{'totalCost'} = sprintf('%.2f', $results{'totalCost'});
}

# Back-calculate "costPerLitre" if necessary
if ($results{'totalCost'} ne 0 and $results{'totalFuel'} ne 0 and $results{'costPerLitre'} eq 0) {
    $results{'costPerLitre'} = $results{'totalCost'} / $results{'totalFuel'};
    $results{'costPerLitre'} = sprintf('%.3f', $results{'costPerLitre'});
}

$output .= "costPerLitre=$results{'costPerLitre'}&";
$output .= "totalFuel=$results{'totalFuel'}&";
$output .= "totalCost=$results{'totalCost'}";

print $output;
