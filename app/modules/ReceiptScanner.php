<?php

require_once realpath(__DIR__ . "/../Bootstrap.php");

use thiagoalessio\TesseractOCR\TesseractOCR;

class ReceiptScanner {
    const TESSERACT_PSM = 6;

    private $imagePath = "";

    public function __construct($imagePath)
    {
        $this->imagePath = $imagePath;
    }

    public function scan()
    {
        $tesseract = new TesseractOCR($this->imagePath);
        $command = "perl " . realpath(SHELL . "/receipt-parse.pl");

        # Extract text
        $text = $tesseract->psm(ReceiptScanner::TESSERACT_PSM)->run();
        $text = preg_replace("/\r|\n|\"|\'|\`|\‘|\’|\”|\“/", "", $text);

        $command .= " \"" . $text . "\"";
        $perlOutput = [];

        # Parse with Perl
        exec($command, $perlOutput);

        # Parse Perl output (url encoded string)
        $outputValues = [];
        parse_str($perlOutput[0], $outputValues);

        return [
            "litres" => floatval(array_key_exists("totalFuel", $outputValues) ? $outputValues["totalFuel"] : 0),
            "cost_per_litre" => floatval(array_key_exists("costPerLitre", $outputValues) ? $outputValues["costPerLitre"] : 0),
            "total_cost" => floatval(array_key_exists("totalCost", $outputValues) ? $outputValues["totalCost"] : 0)
        ];
    }
}
