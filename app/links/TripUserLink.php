<?php

require_once realpath(LINKS . "/AbstractLink.php");
loadModel("Trip");
loadModel("User");

class TripUserLink extends AbstractLink {
    const TABLE_NAME = "trip_user";

    public function __construct () {
        parent::__construct();
    }
    
    public function getTrips (User $user) : array {
        $trips = [];

        $stmt = $this->dbConn->prepare(
            sprintf("
                SELECT t.trip_id
                    ,t.trip_name
                    ,t.trip_desc
                    ,t.start_date
                    ,t.end_date
                    ,t.archived
                FROM `%s` tu
                INNER JOIN `%s` t ON t.trip_id = tu.trip_id
                WHERE tu.user_id = ?
            ", TripUserLink::TABLE_NAME, TripRepository::TABLE_NAME)
        );

        $stmt->execute([$user->getId()]);
        foreach ($stmt->fetchAll() as $tripData) {
            $trips[] = Trip::fromDataSource($tripData);
        }

        return $trips;
    }

    public function getUsers (Trip $trip) : array {
        $users = [];

        $stmt = $this->dbConn->prepare(
            sprintf("
                SELECT u.user_id
                    ,u.display_name
                    ,u.password
                    ,u.color
                    ,u.archived
                    ,u.created_at
                FROM `%s` tu
                INNER JOIN `%s` u ON u.user_id = tu.user_id
                WHERE tu.trip_id = ?
                ORDER BY u.display_name ASC
            ", TripUserLink::TABLE_NAME, UserRepository::TABLE_NAME)
        );

        $stmt->execute([$trip->getId()]);
        foreach ($stmt->fetchAll() as $userData) {
            $users[] = User::fromDataSource($userData);
        }

        return $users;
    }

    public function addLink (Trip $trip, User $user) : bool {
        if ($trip->getId() === null || $user->getId() === null) {
            return false;
        }

        $stmt = $this->dbConn->prepare(
            sprintf("
                INSERT INTO `%s`
                    (`trip_id`, `user_id`)
                VALUES
                    (?, ?)
            ", TripUserLink::TABLE_NAME)
        );

        return $stmt->execute([
            $trip->getId(),
            $user->getId()
        ]);
    }

    public function removeLink (Trip $trip, User $user) : bool {
        if ($trip->getId() === null || $user->getId() === null) {
            return false;
        }

        $stmt = $this->dbConn->prepare(
            sprintf("
                DELETE FROM `%s`
                WHERE 1 = 1
                    AND `trip_id` = ?
                    AND `user_id` = ?
            ", TripUserLink::TABLE_NAME)
        );

        return $stmt->execute([
            $trip->getId(),
            $user->getId()
        ]);
    }
}
