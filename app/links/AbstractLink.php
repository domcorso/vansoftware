<?php

require_once realpath(ROOT . "/Database.php");

abstract class AbstractLink {
    protected $dbConn;

    public function __construct () {
        $this->dbConn = Database::getInstance()->getConn();
    }

    public static function getInstance () {
        static $instance = null;

        if ($instance === null) {
            $instance = new static();
        } return $instance;
    }
}
