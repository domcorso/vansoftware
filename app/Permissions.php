<?php

# Load all models
loadModel("User");
loadModel("Trip");

# Load all links
loadLink("TripUser");

class Permissions {
    private function __construct(App $app)
    {
        $this->app = $app;
    }

    public function getInstance(App $app)
    {
        static $instance = null;
        
        if ($instance === null) {
            $instance = new Permissions($app);
        } return $instance;
    }

    private function isTabletOrAdmin()
    {
        return $this->app->loggedInAsTablet() || ($this->app->getCurrentUser() && $this->app->getCurrentUser()->isAdmin());
    }

    public function canListApps()
    {
        return $this->app->isLoggedIn();
    }

    public function canListTrips()
    {
        return $this->app->isLoggedIn();
    }

    public function canCreateOrUpdateTrips()
    {
        return $this->isTabletOrAdmin();
    }

    public function canListUsers()
    {
        return $this->app->isLoggedIn();
    }

    public function canListFuelEntries()
    {
        return $this->app->isLoggedIn();
    }

    public function canCreateOrUpdateFuelEntries()
    {
        return $this->isTabletOrAdmin();
    }
}
