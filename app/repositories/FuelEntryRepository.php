<?php

require_once realpath(REPOSITORIES . "/AbstractRepository.php");
require_once realpath(MODELS . "/FuelEntry.php");

class FuelEntryRepository extends AbstractRepository {
    const TABLE_NAME = "fuel_entry";

    public function __construct () {
        parent::__construct();
    }

    public function findAll (array $filters = []) : array {
        $fuelEntries = [];

        # Prepare query with support for conditional WHERE clauses
        $stmt = $this->dbConn->prepare(
            sprintf("
                SELECT fuel_entry_id
                    ,litres
                    ,cost_per_litre
                    ,total_cost
                    ,lat
                    ,lng
                    ,trip_id
                    ,entered_by
                    ,photo_file
                    ,created_utc
                FROM `%s`
                WHERE 1 = 1
                    AND ( :fuel_entry_id IS NULL OR `fuel_entry_id` = :fuel_entry_id )
                    AND ( :trip_id IS NULL OR `trip_id` = :trip_id )
                ORDER BY `created_utc` DESC
            ", FuelEntryRepository::TABLE_NAME)
        );

        $normalizedFilters = parent::normalizeFilters($filters, [
            "fuel_entry_id",
            "trip_id"
        ]);

        # Execute query
        $stmt->execute($normalizedFilters);

        foreach ($stmt->fetchAll() as $tripEntryData) {
            $fuelEntries[] = FuelEntry::fromDataSource($tripEntryData);
        }

        return $fuelEntries;
    }

    public function getFuelEntryById (int $fuelEntryId) {
        $fuelEntries = $this->findAll(["fuel_entry_id" => $fuelEntryId]);

        if (count($fuelEntries) === 1) {
            return $fuelEntries[0];
        } return null;
    }

    public function save (FuelEntry $fuelEntry) {
        $validData = $fuelEntry->validate()["valid"];
        $fuelEntryId = $fuelEntry->getId();

        if (!$validData) {
            return false;
        }

        if (empty($fuelEntryId)) {
            return $this->insert($fuelEntry);
        } return $this->update($fuelEntry);
    }

    private function insert (FuelEntry $fuelEntry) {
        $stmt = $this->dbConn->prepare(
            sprintf("
                INSERT INTO `%s`
                    (`litres`, `cost_per_litre`, `total_cost`, `lat`, `lng`, `trip_id`, `entered_by`, `photo_file`)
                VALUES
                    (?, ?, ?, ?, ?, ?, ?, ?)
            ", FuelEntryRepository::TABLE_NAME)
        );
        
        $insertSuccess = $stmt->execute([
            $fuelEntry->getLitres(),
            $fuelEntry->getCostPerLitre(),
            $fuelEntry->getTotalCost(),
            $fuelEntry->getLatitude() == 0 ? null : $fuelEntry->getLatitude(),
            $fuelEntry->getLongitude() == 0 ? null : $fuelEntry->getLongitude(),
            $fuelEntry->getTrip()->getId(),
            $fuelEntry->getEnteredBy()->getId(),
            $fuelEntry->getPhotoFile()
        ]);

        if ($insertSuccess) {
            return $this->dbConn->lastInsertId();
        } return false;
    }

    private function update (FuelEntry $fuelEntry) {
        $stmt = $this->dbConn->prepare(
            sprintf("
                UPDATE `%s`
                SET
                    `litres` = ?,
                    `cost_per_litre` = ?,
                    `total_cost` = ?,
                    `lat` = ?,
                    `lng` = ?,
                    `trip_id` = ?,
                    `entered_by` = ?,
                    `photo_file` = ?
                WHERE `fuel_entry_id` = ?
            ", FuelEntryRepository::TABLE_NAME)
        );
        
        $updateSuccess = $stmt->execute([
            $fuelEntry->getLitres(),
            $fuelEntry->getCostPerLitre(),
            $fuelEntry->getTotalCost(),
            $fuelEntry->getLatitude() == 0 ? null : $fuelEntry->getLatitude(),
            $fuelEntry->getLongitude() == 0 ? null : $fuelEntry->getLongitude(),
            $fuelEntry->getTrip()->getId(),
            $fuelEntry->getEnteredBy()->getId(),
            $fuelEntry->getPhotoFile(),
            $fuelEntry->getId()
        ]);

        if ($updateSuccess) {
            return $fuelEntry->getId();
        } return false;
    }
}
