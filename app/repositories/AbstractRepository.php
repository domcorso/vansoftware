<?php

require_once realpath(ROOT . "/Database.php");

abstract class AbstractRepository {
    protected $dbConn;

    public function __construct () {
        $this->dbConn = Database::getInstance()->getConn();
    }

    public static function getInstance () {
        static $instance = null;

        if ($instance === null) {
            $instance = new static();
        } return $instance;
    }

    # Makes a copy of $requiredKeys as an associative array, containing
    # the corresponding value within $filters. If there is no value found
    # for that key in $filters, then it will be set to null.
    # -------------------------------------------------------------
    # USED FOR PASSING IN PARAMETERS WITH CONDITIONAL WHERE CLAUSES
    protected static function normalizeFilters ($filters, $requiredKeys) {
        $normalizedFilters = [];

        foreach ($requiredKeys as $rk) {
            $normalizedFilters[$rk] = array_key_exists($rk, $filters) ? $filters[$rk] : null;
        } return $normalizedFilters;
    }
}
