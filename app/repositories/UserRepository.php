<?php

require_once realpath(REPOSITORIES . "/AbstractRepository.php");
require_once realpath(MODELS . "/User.php");

class UserRepository extends AbstractRepository {
    const TABLE_NAME = "user";

    public function __construct () {
        parent::__construct();
    }

    public function findAll (array $filters = []) : array {
        $users = [];

        # Prepare query with support for conditional WHERE clauses
        $stmt = $this->dbConn->prepare(
            sprintf("
                SELECT user_id
                    ,display_name
                    ,password
                    ,color
                    ,archived
                    ,UNIX_TIMESTAMP(created_at) AS created_at
                FROM `%s`
                WHERE 1 = 1
                    AND ( :user_id IS NULL OR `user_id` = :user_id )
                ORDER BY `display_name` ASC
            ", UserRepository::TABLE_NAME)
        );

        $normalizedFilters = parent::normalizeFilters($filters, [
            "user_id"
        ]);

        # Execute query
        $stmt->execute($normalizedFilters);

        foreach ($stmt->fetchAll() as $userData) {
            $users[] = User::fromDataSource($userData);
        }

        return $users;
    }

    public function getUserById (int $userId) {
        $users = $this->findAll(["user_id" => $userId]);

        if (count($users) === 1) {
            return $users[0];
        } return null;
    }

    public function save (User $user) {
        $validData = $user->validate()["valid"];
        $userId = $user->getId();

        if (!$validData) {
            return false;
        }

        if (empty($userId)) {
            return $this->insert($user);
        } return $this->update($user);
    }

    private function insert (User $user) {
        $stmt = $this->dbConn->prepare(
            sprintf("
                INSERT INTO `%s`
                    (`display_name`, `password`, `color`)
                VALUES
                    (?, ?, ?)
            ", UserRepository::TABLE_NAME)
        );
        
        $insertSuccess = $stmt->execute([
            $user->getDisplayName(),
            $user->getPassword(),
            $user->getColor()
        ]);

        if ($insertSuccess) {
            return $this->dbConn->lastInsertId();
        } return false;
    }

    private function update (User $user) {
        $stmt = $this->dbConn->prepare(
            sprintf("
                UPDATE `%s`
                SET
                    `display_name` = ?,
                    `password` = ?,
                    `color` = ?,
                    `archived` = ?
                WHERE `user_id` = ?
            ", UserRepository::TABLE_NAME)
        );
        
        $updateSuccess = $stmt->execute([
            $user->getDisplayName(),
            $user->getPassword(),
            $user->getColor(),
            $user->isArchived(),
            $user->getId()
        ]);

        if ($updateSuccess) {
            return $user->getId();
        } return false;
    }

    public function isDisplayNameUsed ($displayName) : bool {
        $stmt = $this->dbConn->prepare(
            sprintf("
                SELECT 1
                FROM `%s`
                WHERE `display_name` = ?
            ", UserRepository::TABLE_NAME)
        );

        $stmt->execute([$displayName]);

        return $stmt->rowCount() > 0;
    }
}
