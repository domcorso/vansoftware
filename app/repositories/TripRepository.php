<?php

require_once realpath(REPOSITORIES . "/AbstractRepository.php");
require_once realpath(MODELS . "/Trip.php");

class TripRepository extends AbstractRepository {
    const TABLE_NAME = "trip";
    const SELECT_COLS = ["trip_id", "trip_name", "trip_desc", "start_date", "end_date", "archived"];

    private $selectStr = "";

    public function __construct () {
        parent::__construct();
        $this->selectStr = implode(self::SELECT_COLS, ",");
    }

    public function findAll (array $filters = []) {
        $trips = [];

        # Prepare query with support for conditional WHERE clauses
        $stmt = $this->dbConn->prepare(
            sprintf("
                SELECT %s FROM `%s`
                WHERE 1 = 1
                    AND ( :trip_id IS NULL OR `trip_id` = :trip_id )
                ORDER BY `start_date` DESC
            ", $this->selectStr, self::TABLE_NAME)
        );

        $normalizedFilters = parent::normalizeFilters($filters, [
            "trip_id"
        ]);

        # Execute query
        $stmt->execute($normalizedFilters);

        foreach ($stmt->fetchAll() as $tripData) {
            $trips[] = Trip::fromDataSource($tripData);
        }

        return $trips;
    }

    public function getTripById (int $tripId) {
        $trips = $this->findAll(["trip_id" => $tripId]);

        if (count($trips) === 1) {
            return $trips[0];
        } return null;
    }

    public function save (Trip $trip) {
        $validForInsert = $trip->validate()["valid"];
        $tripId = $trip->getId();

        if (!$validForInsert) {
            return false;
        }

        if (empty($tripId)) {
            return $this->insert($trip);
        } return $this->update($trip);
    }

    private function insert (Trip $trip) {
        $stmt = $this->dbConn->prepare(
            sprintf("
                INSERT INTO `%s`
                    (`trip_name`, `trip_desc`, `start_date`, `end_date`, `archived`)
                VALUES
                    (?, ?, ?, ?, 0)
            ", self::TABLE_NAME)
        );

        # 'end_date' is null-able
        $endDate = $trip->getEndDate();

        if ($endDate instanceof DateTime) {
            $endDate = $endDate->getTimestamp();
        } else {
            $endDate = null;
        }

        $insertSuccess = $stmt->execute([
            $trip->getName(),
            $trip->getDescription(),
            $trip->getStartDate()->getTimestamp(),
            $endDate
        ]);

        if ($insertSuccess) {
            return $this->dbConn->lastInsertId();
        } return false;
    }

    private function update (Trip $trip) {
        $stmt = $this->dbConn->prepare(
            sprintf("
                UPDATE `%s`
                SET
                    `trip_name`=?,
                    `trip_desc`=?,
                    `start_date`=?,
                    `end_date`=?,
                    `archived`=?
                WHERE trip_id = ?
            ", self::TABLE_NAME)
        );

        # 'end_date' is null-able
        $endDate = $trip->getEndDate();

        if ($endDate instanceof DateTime) {
            $endDate = $endDate->getTimestamp();
        } else {
            $endDate = null;
        }

        $updateSuccess = $stmt->execute([
            $trip->getName(),
            $trip->getDescription(),
            $trip->getStartDate()->getTimestamp(),
            $endDate,
            $trip->isArchived(),
            $trip->getId()
        ]);

        if ($updateSuccess) {
            return $trip->getId();
        } return false;
    }
}
