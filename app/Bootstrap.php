<?php

define("SESSION_MAX_LIFETIME", 2 * 86400);
ini_set('session.gc_maxlifetime', SESSION_MAX_LIFETIME);
session_set_cookie_params(SESSION_MAX_LIFETIME);
session_start();

date_default_timezone_set("Australia/Melbourne");

# Globally used constants
define("ROOT", realpath(__DIR__));
define("RESOURCES", realpath(ROOT . "/../resources"));
define("DATA", realpath(ROOT . "/../data"));
define("MODELS", realpath(ROOT . "/models"));
define("REPOSITORIES", realpath(ROOT . "/repositories"));
define("MODULES", realpath(ROOT . "/modules"));
define("SHELL", realpath(ROOT . "/shell"));
define("LINKS", realpath(ROOT . "/links"));
define("PUB_TABLET", realpath(ROOT . "/../public-tablet"));
define("PUB_MOBILE", realpath(ROOT . "/../public-mobile"));
define("DATETIME_FORMAT", "Y-m-d H:i:s");

define("FORM_NEUTRAL", "form neutral");
define("FORM_ERROR", "form error");
define("FORM_SUCCESS", "form success");

# 4.9 for some extra padding, but really 5MB
define("MAX_UPLOAD_SIZE", 1000000 * 4.9);

# Other
require ROOT . "/App.php";

# Composer autoload
require ROOT . "/vendor/autoload.php";

# Application instance
$app = App::getInstance();

function loadModel (string $modelName) {
    require_once realpath(MODELS . "/AbstractModel.php");
    require_once realpath(MODELS . "/{$modelName}.php");
    require_once realpath(REPOSITORIES . "/{$modelName}Repository.php");
}

function loadLink (string $linkName) {
    require_once realpath(LINKS . "/{$linkName}Link.php");
}

# Returns a copy of $_GET removing any keys that are
# not within $allowedKeys
function cleanGET (array $allowedKeys) {
    return array_filter($_GET, function($key) use ($allowedKeys) {
        return in_array($key, $allowedKeys);
    }, ARRAY_FILTER_USE_KEY);
}

# Returns true/false depending on if a given model
# is provided in the $_GET["link"] query string
function linkOn ($model) : bool {
    $links = isset($_GET["link"]) ? explode(",", $_GET["link"]) : [];
    $links = array_map("strtolower", $links);
    $model = strtolower($model);
    return in_array($model, $links);
}
