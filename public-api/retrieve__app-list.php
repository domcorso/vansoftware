<?php

require_once realpath(__DIR__ . "/../app/Bootstrap.php");

if ($app->permissions->canListApps()) {
    header("Content-type: application/json");
    $appListPath = RESOURCES . "/apps/app-list.json";
    echo file_get_contents($appListPath);
} else {
    http_response_code(401);
}
