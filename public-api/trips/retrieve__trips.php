<?php

require_once realpath(__DIR__ . "/../../app/Bootstrap.php");
loadModel("Trip");
loadLink("TripUser");

if ($app->permissions->canListTrips()) {
    header("Content-type: application/json");

    $tripRepository = TripRepository::getInstance();
    $tripUserLink = TripUserLink::getInstance();
    $filters = cleanGET(["trip_id"]);

    $trips = $tripRepository->findAll($filters);

    foreach ($trips as $i => $trip) {
        $assoc = $trip->jsonSerialize();

        if (linkOn("user")) {
            $assoc["users"] = $tripUserLink->getUsers($trip);
        }

        $trips[$i] = $assoc;
    }

    echo json_encode($trips);
} else {
    http_response_code(401);
}
