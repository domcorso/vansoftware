<?php

require_once realpath(__DIR__ . "/../../app/Bootstrap.php");
loadModel("User");
loadModel("Trip");
loadLink("TripUser");

if ($app->permissions->canCreateOrUpdateTrips()) {
    header("Content-type: application/json");

    $userRepository = UserRepository::getInstance();
    $tripRepository = TripRepository::getInstance();
    $tripUserLink = TripUserLink::getInstance();
    
    # Response
    $response = [
        "errors" => []
    ];
    
    # Get JSON body
    $jsonBody = file_get_contents("php://input", true);
    $payload = json_decode($jsonBody, JSON_OBJECT_AS_ARRAY);
    
    # Abort if unable to parse or wrong format
    if (is_null($payload)) {
        http_response_code(400);
        exit();
    }

    $tripId = $payload["tripId"];
    $users = $payload["users"];

    $trips = $tripRepository->findAll(["trip_id" => $tripId]);

    if (count($trips) === 0) {
        $response["errors"] = ["Trip does not exist with the ID: {$tripId}"];
        echo json_encode($response);
        exit();
    }

    $trip = $trips[0];
    
    # "userActionPair" == { userId: 0, action: "add/remove"}
    foreach ($users as $userActionPair) {
        $userId = $userActionPair["userId"];
        $action = $userActionPair["action"];
        $user = $userRepository->findAll(["user_id" => $userId]);

        if (count($user) === 1) {
            $user = $user[0];

            switch ($action) {
                case "add":
                    $tripUserLink->addLink($trip, $user);
                    break;
                case "remove":
                    $tripUserLink->removeLink($trip, $user);
                    break;
                default:
                    $response["errors"][] = "Invalid action: ${$action}";
                    break;
            }
        } else {
            $response["errors"][] = "Cannot find User with ID: {$userId}";
        }
    }

    echo json_encode($response);
} else {
    http_response_code(401);
}
