<?php

require_once realpath(__DIR__ . "/../../app/Bootstrap.php");
loadModel("Trip");

if ($app->permissions->canCreateOrUpdateTrips()) {
    header("Content-type: application/json");

    $tripRepository = TripRepository::getInstance();
    
    # Response
    $response = [
        "errors" => [],
        "tripId" => 0
    ];
    
    # Get JSON body
    $jsonBody = file_get_contents("php://input", true);
    $tripData = json_decode($jsonBody, JSON_OBJECT_AS_ARRAY);
    
    # Abort if unable to parse or wrong format
    if (is_null($tripData)) {
        http_response_code(400);
        exit();
    }
    
    # Required fields and convert to string for ease of use
    foreach (["tripId", "tripName", "tripDesc", "startDate", "endDate"] as $requiredField) {
        if (!array_key_exists($requiredField, $tripData)) {
            http_response_code(400);
            exit();
        }
    
        $tripData[$requiredField] = (string) $tripData[$requiredField];
    }
    
    $tripData["startDate"] = DateTime::createFromFormat("!d/m/Y", $tripData["startDate"]);
    $tripData["endDate"] = DateTime::createFromFormat("!d/m/Y", $tripData["endDate"]);
    
    $findResults = $tripRepository->findAll([
        "trip_id" => $tripData["tripId"]
    ]);
    
    # Get Trip by ID
    $trip = count($findResults) === 1 ? $findResults[0] : null;
    
    if (is_null($trip)) {
        $response["errors"] = ["Cannot find trip with that ID"];
        echo json_encode($response);
        die();
    }
    
    $trip->setName($tripData["tripName"]);
    $trip->setDescription($tripData["tripDesc"]);
    
    if ($tripData["startDate"] !== false) {
        $trip->setStartDate($tripData["startDate"]);
    }
    
    if ($tripData["endDate"] !== false) {
        $trip->setEndDate($tripData["endDate"]);
    }
    
    $response["tripId"] = (int) $trip->getId();
    
    # Save this Trip
    $saved = $tripRepository->save($trip);
    if ($saved === false) {
        $response["errors"] = $trip->validate()["messages"];
    }
    
    echo json_encode($response);
} else {
    http_response_code(401);
}
