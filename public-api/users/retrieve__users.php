<?php

require_once realpath(__DIR__ . "/../../app/Bootstrap.php");
loadModel("User");

if ($app->permissions->canListUsers()) {
    header("Content-type: application/json");
    
    $userRepository = UserRepository::getInstance();
    $filters = cleanGET(["user_id"]);
    
    echo json_encode(
        $userRepository->findAll($filters)
    );
} else {
    http_response_code(401);
}
