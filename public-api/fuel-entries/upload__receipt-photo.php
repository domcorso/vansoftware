<?php

require_once realpath(__DIR__ . "/../../app/Bootstrap.php");
require_once realpath(MODULES . "/ReceiptScanner.php");
loadModel("FuelEntry");
use Imagecow\Image;

header("Content-Type: application/json");

define("FILE_KEY", "receipt_photo");
define("ALLOWED_EXTENSIONS", ["jpg"]);

$response = [
    "errors" => [],
    "uploaded_url" => "",
    "scanned_values" => [
        "litres" => 0,
        "cost_per_litre" => 0,
        "total_cost" => 0
    ]
];

if (isset($_FILES[FILE_KEY])) {
    $newUrl = "";
    $errors = [];
    $scannedValues = [
        "litres" => 0,
        "cost_per_litre" => 0,
        "total_cost" => 0
    ];

    $fileSize = $_FILES[FILE_KEY]["size"];
    $fileTemp = $_FILES[FILE_KEY]["tmp_name"];
    $exploded = explode(".", $_FILES[FILE_KEY]["name"]);
    $fileExtn = strtolower(end($exploded));

    if (!in_array($fileExtn, ALLOWED_EXTENSIONS)) {
        $errors[] = "Incorrect file type. Supported include: " . implode(", ", ALLOWED_EXTENSIONS);
    }

    if ($fileSize > MAX_UPLOAD_SIZE) {
        $errors[] = "File size too big. Maximum is " . MAX_UPLOAD_SIZE / 1000000 . "MB";
    }

    if (empty($errors)) {
        $uniqueId = "receipt_" . hash("sha1", uniqid(time(), true) . $fileSize);
        $filename = $uniqueId . "." . $fileExtn;
        $newUrl = FuelEntry::getReceiptPhotoPublicPrefix() . "/{$filename}";
        $path = FuelEntry::getReceiptPhotoDirectory() . "/{$filename}";

        # Move to public uploads
        move_uploaded_file($fileTemp, $path);
        
        # Resize for quick scanning
        $image = Image::fromFile($path);
        $image->resize(800);
        $image->autoRotate();
        $image->save();

        # Scan/parse receipt photo for value extraction
        $scanner = new ReceiptScanner($path);
        $scanResult = $scanner->scan();

        $scannedValues["litres"] = $scanResult["litres"];
        $scannedValues["cost_per_litre"] = $scanResult["cost_per_litre"];
        $scannedValues["total_cost"] = $scanResult["total_cost"];

    }

    $response["errors"] = $errors;
    $response["uploaded_url"] = $newUrl;
    $response["filename"] = $filename;
    $response["scanned_values"] = $scannedValues;
}

echo json_encode($response);
