<?php

require_once realpath(__DIR__ . "/../../app/Bootstrap.php");
loadModel("FuelEntry");

if ($app->permissions->canListFuelEntries()) {
    header("Content-type: application/json");
    
    $fuelEntryRepository = FuelEntryRepository::getInstance();
    $filters = cleanGET(["fuel_entry_id", "trip_id"]);
    
    echo json_encode(
        $fuelEntryRepository->findAll($filters)
    );
} else {
    http_response_code(401);
}
